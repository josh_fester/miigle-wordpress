<?php
/**
 * Dependencies required
 */
require_once("wp-load.php");
use Miigle\Models\Product;


/**
 * Variables needed
 */
$scope = '';
$queryScope = '';

/**
 * Checks if the scope[category,persona] parameter is set and if it is readies it for query
 */
if (isset($_GET['productID']))
{
    $scope =  $_GET['productID'] ;
    $scope = str_replace('\"', '', $scope);
    if ($scope != 'all')
        $queryScope = $scope;
}

$data = [];

$categories = Product\get_categories($queryScope);
$filters = '';
foreach ($categories as $category) {

    $filters .= $category->slug . ',';
}
$filters = substr($filters, 0, -1);

$the_query = new WP_Query(['post_type' => 'mgl_product', 'mgl_product_category' => $filters, 'orderby' => 'rand', 'post_status' => 'publish']);

if ($the_query->have_posts()) {
    while ($the_query->have_posts()){
        $the_query->the_post();

        $row['brand'] = Product\get_brand_title(get_the_ID());
        $row['price'] = Product\get_price(get_the_ID());
        $row['thumbnail'] = Product\get_thumbnail(get_the_ID());
        $row['url'] = get_the_permalink();
        $row['title'] = get_the_title();
        $data[] = $row;

    }
}
/**
 * Formats the data so it has everything needed for the JS
 */

/**
 * Returns data as JSON
 */
echo json_encode($data);

