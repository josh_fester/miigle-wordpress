<?php
/**
 * Dependencies required
 */
require_once("wp-load.php");
use Miigle\Models\Product;

/**
 * DB connection
 */
$connection = \mysqli_connect('miigle-wp.czbchjvo0lqj.us-west-1.rds.amazonaws.com', 'miigle', 'Tower1999!?', 'miigle_dev');

/**
 * Variables needed
 */
$scope = '';
$queryScope = '';
$sortScope = 'RAND()';
$pageScope = 0;
/**
 * Checks if the sort parameter is set and if it is readies it for query
 */
if (isset($_GET['sort']))
{
    $temp = $_GET['sort'];
    $temp = str_replace('\"', '', $temp);
    if ($temp == 'newest')
        $sortScope = 'wp_posts.post_date desc';
}
/**
 * Checks if the scope[category,persona] parameter is set and if it is readies it for query
 */
if (isset($_GET['scope']))
{
    $scope =  $_GET['scope'] ;
    $scope = str_replace('\"', '', $scope);
    if ($scope != 'all')
        $queryScope = "AND wp_terms.slug = '" . $scope . "'";
}
/**
 * Checks if the page parameter is set and if it is readies it for query
 */
if (isset($_GET['page']))
{
    $pageScope = $_GET['page'];
    $pageScope = str_replace('\"', '', $pageScope);
    $pageScope = 120 * $pageScope;
}
/**
 * Main ajax query
 */
$query = "
    SELECT wp_posts.id, wp_posts.post_date, wp_posts.post_title, wp_posts.guid, wp_terms.slug
    FROM wp_posts 
    JOIN wp_term_relationships ON wp_term_relationships.object_id = wp_posts.id 
    JOIN wp_terms ON wp_terms.term_id = wp_term_relationships.term_taxonomy_id
    WHERE post_type = 'mgl_product'
      AND post_status = 'publish'
      {$queryScope}
    ORDER BY {$sortScope}
    LIMIT {$pageScope},120 
  
";
$amountQuery = "
    SELECT wp_posts.id
    FROM wp_posts 
    JOIN wp_term_relationships ON wp_term_relationships.object_id = wp_posts.id 
    JOIN wp_terms ON wp_terms.term_id = wp_term_relationships.term_taxonomy_id
    WHERE post_type = 'mgl_product'
      AND post_status = 'publish'
      {$queryScope}
    ORDER BY {$sortScope}
    LIMIT 9999999 
  
";
/**
 * Returns the database response and saves it in $result
 */
$amount = $connection->query($amountQuery);
$counter = 0;
while($results = $amount->fetch_assoc())
{
    $counter++;
}
$data = [];
/**
 * Formats the data so it has everything needed for the JS
 */

$result = $connection->query($query);
while($row = $result->fetch_assoc())
{
    $row['post_title'] = get_the_title($row['id']);
    $row['brand'] = Product\get_brand_title($row['id']);
    $row['price'] = Product\get_price($row['id']);
    $row['thumbnail'] = Product\get_thumbnail($row['id']);
    $data[] = $row;

}
$data['counter'] = $counter;
/**
 * Returns data as JSON
 */
echo json_encode($data);
