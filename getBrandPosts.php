<?php
/**
 * Dependencies required
 */
require_once("wp-load.php");
use Miigle\Models\Product;

/**
 * Variables needed
 */
$category = '';
$brandId = '';
/**
 * Checks if the scope[category,persona] parameter is set and if it is readies it for query
 */
if (isset($_GET['category']))
{
    $category =  $_GET['category'] ;
    $category = str_replace('\"', '', $category);

}
/**
 * Checks if the brandID parameter is set and if it is readies it for query
 */
if (isset($_GET['brandID']))
{
    $brandId =  $_GET['brandID'] ;
    $brandId = str_replace('\"', '', $brandId);

}


$data = [];
$args = array('post_type' => 'mgl_product', 'meta_key' => '_mgl_product_brand_id', 'meta_value' => $brandId, 'mgl_product_category' => $category, 'posts_per_page' => -1, 'post_status' => 'publish');
$the_query = new WP_Query($args);
if ($the_query->have_posts()) {
    while ($the_query->have_posts()){
        $the_query->the_post();
        $row['brand'] = Product\get_brand_title(get_the_ID());
        $row['price'] = Product\get_price(get_the_ID());
        $row['thumbnail'] = Product\get_thumbnail(get_the_ID());
        $row['url'] = get_the_permalink();
        $row['title'] = get_the_title();
        $data[] = $row;
    }
}

/**
 * Returns data as JSON
 */
echo json_encode($data);
