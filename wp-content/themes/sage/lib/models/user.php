<?php

namespace Miigle\Models\User;

use Miigle\Models\Brand;
use Miigle\Models\Product;

/**
 * Get a user and their meta
 */
function get( $id ) {
	$user    = new \WP_User( $id );
	$avatar  = get_user_meta( $id, 'thechamp_large_avatar', true );
	$socials = get_user_meta( $id, 'thechamp_linked_accounts', true );
	$socials = unserialize( $socials );

	return array(
		'ID'                 => $id,
		'username'           => get_username( $id ),
		'email'              => $user->user_email,
		'first_name'         => $user->user_firstname,
		'last_name'          => $user->user_lastname,
		'full_name'          => $user->user_firstname . ' ' . $user->user_lastname,
		'website'            => $user->user_url,
		'avatar'             => $avatar,
		'_mgl_user_title'    => get_title( $id ),
		'_mgl_user_website'  => get_website( $id ),
		'_mgl_user_facebook' => get_facebook( $id ),
		'_mgl_user_twitter'  => get_twitter( $id ),
		'_mgl_user_company'  => get_company( $id )
	);
}

/**
 * Get the current user
 */
function current() {
	return get( wp_get_current_user()->ID );
}

add_action( 'cmb2_admin_init', __NAMESPACE__ . '\\register_meta' );
add_action( 'rest_api_init', __NAMESPACE__ . '\\api_register_meta' );
/**
 * Define the metabox and field configurations.
 */
function register_meta() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_mgl_user_';

	/**
	 * Initiate the metabox
	 */
	$cmb = new_cmb2_box( array(
		'id'           => $prefix,
		'title'        => __( 'User Fields', 'cmb2' ),
		'object_types' => array( 'user' ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );

	// Brand ID
	$cmb->add_field( array(
		'name' => __( 'Title & Company', 'cmb2' ),
		'desc' => __( '', 'cmb2' ),
		'id'   => $prefix . 'title',
		'type' => 'text'
	) );

	// Website
	$cmb->add_field( array(
		'name' => __( 'Website', 'cmb2' ),
		'desc' => __( '', 'cmb2' ),
		'id'   => $prefix . 'website',
		'type' => 'text_url'
	) );

	// Braintree ID
//  $cmb->add_field(array(
//    'name'       => __('Braintree ID', 'cmb2'),
//    'desc'       => __('', 'cmb2'),
//    'id'         => $prefix . 'braintree_id',
//    'type'       => 'text'
//  ));

}

function api_register_meta() {

	register_rest_field(
		'user',
		'first_name',
		array(
			'get_callback'    => 'Miigle\\Models\\Model\\get_miigle_user_slug',
			'update_callback' => 'Miigle\\Models\\Model\\update_miigle_user_slug',
			'schema'          => array(
				'description' => 'User First Name',
				'type'        => 'string',
				'context'     => [ 'view', 'edit' ],
			),
		)
	);

	register_rest_field(
		'user',
		'last_name',
		array(
			'get_callback'    => 'Miigle\\Models\\Model\\get_miigle_user_slug',
			'update_callback' => 'Miigle\\Models\\Model\\update_miigle_user_slug',
			'schema'          => array(
				'description' => 'User Last Name',
				'type'        => 'string',
				'context'     => [ 'view', 'edit' ],
			),
		)
	);

	register_rest_field(
		'user',
		'user_email',
		array(
			'get_callback'    => __NAMESPACE__ . '\\get_miigle_user_data',
			'update_callback' => __NAMESPACE__ . '\\update_miigle_user_data',
			'schema'          => array(
				'description' => 'User Email Address',
				'type'        => 'string',
				'context'     => [ 'view', 'edit' ],
			),
		)
	);

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_mgl_user_';

	register_rest_field(
		'user',
		$prefix . 'title',
		array(
			'get_callback'    => 'Miigle\\Models\\Model\\get_miigle_user_slug',
			'update_callback' => 'Miigle\\Models\\Model\\update_miigle_user_slug',
			'schema'          => array(
				'description' => 'User Title',
				'type'        => 'string',
				'context'     => [ 'view', 'edit' ],
			),
		)
	);

	register_rest_field(
		'user',
		$prefix . 'website',
		array(
			'get_callback'    => 'Miigle\\Models\\Model\\get_miigle_user_slug',
			'update_callback' => 'Miigle\\Models\\Model\\update_miigle_user_slug',
			'schema'          => array(
				'description' => 'User Brand Website',
				'type'        => 'string',
				'context'     => [ 'view', 'edit' ],
			),
		)
	);

	register_rest_field(
		'user',
		'thechamp_avatar',
		array(
			'get_callback'    => 'Miigle\\Models\\Model\\get_miigle_user_slug',
			'update_callback' => 'Miigle\\Models\\Model\\update_miigle_user_slug',
			'schema'          => array(
				'description' => 'Social Avatar URL',
				'type'        => 'string',
				'format'      => 'uri',
				'context'     => [ 'view', 'edit' ],
			),
		)
	);

	register_rest_field(
		'user',
		'thechamp_large_avatar',
		array(
			'get_callback'    => 'Miigle\\Models\\Model\\get_miigle_user_slug',
			'update_callback' => 'Miigle\\Models\\Model\\update_miigle_user_slug',
			'schema'          => array(
				'description' => 'Large Social Avatar URL',
				'type'        => 'string',
				'format'      => 'uri',
				'context'     => [ 'view', 'edit' ],
			),
		)
	);

	register_rest_field(
		'user',
		'wp_metronet_image_id',
		array(
			'get_callback'    => __NAMESPACE__ . '\\get_user_image',
			'update_callback' => 'Miigle\\Models\\Model\\update_miigle_user_slug',
			'schema'          => array(
				'description' => 'Custom User Profile Image',
				'type'        => 'image',
				'context'     => [ 'view', 'edit' ],
			),
		)
	);
}

/**
 * Get user title
 */
function get_title( $user_id ) {
	return get_user_meta( $user_id, '_mgl_user_title', true );
}

/**
 * Get user avatar
 */
function get_avatar( $user_id ) {
	return get_user_meta( $user_id, 'thechamp_large_avatar', true );
}

/**
 * Get user website
 */
function get_website( $user_id ) {
	return get_user_meta( $user_id, '_mgl_user_website', true );
}

/**
 * Get user facebook
 */
function get_facebook( $user_id ) {
	return get_user_meta( $user_id, '_mgl_user_facebook', true );
}

/**
 * Get user twitter
 */
function get_twitter( $user_id ) {
	return get_user_meta( $user_id, '_mgl_user_twitter', true );
}

/**
 * Get user company
 */
function get_company( $user_id ) {
	return get_user_meta( $user_id, '_mgl_user_company', true );
}

/**
 * Get username
 */
function get_username( $user_id ) {
	$username = get_user_meta( $user_id, '_mgl_user_username', true );
	if ( $username ) {
		return $username;
	} else {
		$userdata = get_userdata( $user_id );
		if ( $userdata ) {
			return $userdata->user_login;
		} else {
			return '';
		}
	}
}

/**
 * Get array of upvoted post id's
 */
function get_upvotes( $user_id, $post_type = 'mgl_product' ) {
	if ( $post_type == 'mgl_brand' ) {
		return get_user_meta( $user_id, '_mgl_user_upvotes_brand', true );
	} else {
		return get_user_meta( $user_id, '_mgl_user_upvotes_product', true );
	}
}

/**
 * Get a count of upvotes
 */
function get_upvotes_count( $user_id, $post_type = 'mgl_product' ) {
	$upvoted = get_upvotes( $user_id, $post_type );

	if ( $upvoted && is_array( $upvoted ) ) {
		return count( $upvoted );
	} else {
		return 0;
	}
}

/**
 * Get a count of upvotes
 */
function get_product_upvotes_count( $user_id, $post_type = 'mgl_product' ) {
	return get_upvotes_count( $user_id, 'mgl_product' );
}

/**
 * Check if a user has upvoted a product
 */
function has_upvoted( $user_id, $post_id, $post_type = 'mgl_product' ) {
	$upvoted = get_upvotes( $user_id, $post_type );

	if ( $upvoted && is_array( $upvoted ) ) {
		if ( array_search( $post_id, $upvoted ) === false ) {
			return false;
		} else {
			return true;
		}
	}

	return false;
}

/**
 * Check if a user has upvoted a product
 */
function has_upvoted_product( $user_id, $post_id ) {
	return has_upvoted( $user_id, $post_id, 'mgl_product' );
}

/**
 * Upvote a product
 */
function upvote( $user_id, $post_id, $post_type = 'mgl_product' ) {
	$upvoted = get_upvotes( $user_id, $post_type );
	if ( $post_type == 'mgl_brand' ) {
		$meta_key = '_mgl_user_upvotes_brand';
	} else {
		$meta_key = '_mgl_user_upvotes_product';
	}

	if ( $upvoted && is_array( $upvoted ) ) {
		array_push( $upvoted, $post_id );
	} else {
		$upvoted = array( $post_id );
	}

	return update_user_meta( $user_id, $meta_key, $upvoted );
}

/**
 * Upvote a product
 */
function upvote_product( $user_id, $post_id ) {
	return upvote( $user_id, $post_id, 'mgl_product' );
}

/**
 * Upvote a brand
 */
function upvote_brand( $user_id, $post_id ) {
	return upvote( $user_id, $post_id, 'mgl_brand' );
}

/**
 * Downvote a product
 */
function downvote( $user_id, $post_id, $post_type = 'mgl_product' ) {
	$upvoted = get_upvotes( $user_id, $post_type );
	if ( $post_type == 'mgl_brand' ) {
		$meta_key = '_mgl_user_upvotes_brand';
	} else {
		$meta_key = '_mgl_user_upvotes_product';
	}

	if ( $upvoted && is_array( $upvoted ) ) {
		$key = array_search( $post_id, $upvoted );
		array_splice( $upvoted, $key, 1 );

		return update_user_meta( $user_id, $meta_key, $upvoted );
	}

	return true;
}

/**
 * Downvote a product
 */
function downvote_product( $user_id, $post_id ) {
	return downvote( $user_id, $post_id, 'mgl_product' );
}

/**
 * Downvote a brand
 */
function downvote_brand( $user_id, $post_id ) {
	return downvote( $user_id, $post_id, 'mgl_brand' );
}

/**
 * Get user's submitted brands and products, and latest submitted brands.
 *
 * @param $user WP User object
 *
 * @return array User activity and recent brands
 */
function activity( $user ) {
	$pending_brands = \get_posts( array(
		'post_type'   => 'mgl_brand',
		'post_status' => [ 'pending', 'published' ],
		'nopaging'    => true,
		'author'      => $user->ID,
	) );

	$latest_brands = \get_posts( array(
		'post_type'  => 'mgl_brand',
		'nopaging'   => true,
		'date_query' => array(
			array(
				'after'     => date( 'Y-m-d', strtotime( '-14 days' ) ),
				'inclusive' => true,
			),
		),
	) );

	$pending_products = \get_posts( array(
		'post_type'   => 'mgl_product',
		'post_status' => [ 'pending', 'published' ],
		'nopaging'    => true,
		'author'      => $user->ID,
	) );

	$brands = array_merge( $pending_brands, $latest_brands );

	// get random brand thumbnails
	for ( $i = 0; $i < count( $brands ); $i ++ ) {
		$brands[ $i ]->thumbnail = Brand\get_brand_thumbnails( $brands[ $i ]->ID, [ 'count' => 1 ] );
	}

	// get product thumbnails
	for ( $i = 0; $i < count( $pending_products ); $i ++ ) {
		$pending_products[ $i ]->thumbnail = Product\get_thumbnail( $pending_products[ $i ]->ID, '_mgl_product_image_gallery', null, null );
	}

	$result = array_merge( $brands, $pending_products );

	// sort by date
	usort( $result, function ( $a, $b ) {
		return $a->post_date <= $b->post_date;
	} );

	return $result;
}

function get_miigle_user_data( $object, $field_name, $request ) {
	return get_userdata( $object['id'] )->{$field_name};
}

function update_miigle_user_data( $value, $object, $field_name ) {
	$object->{$field_name} = $value;

	return wp_update_user( $object );
}

function get_user_image( $object, $field_name, $request ) {
	if ( $image['id'] = get_user_meta( $object['id'], $field_name, true ) ) {

		$image_meta['media_details'] = wp_get_attachment_metadata( $image['id'] );
		$image                       = array_merge( $image, $image_meta );

		return $image;
	}

	return false;
}