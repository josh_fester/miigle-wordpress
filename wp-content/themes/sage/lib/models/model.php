<?php

namespace Miigle\Models\Model;

use Miigle\Models\Product;
use Miigle\Models\User;
use Miigle\Models\Brand;

/**
 * Upvote
 */
function upvote($post_id, $user_id, $post_type, $meta_key_prefix) {
  if($post_type == 'mgl_brand') {
    $upvotes = Brand\get_upvotes($post_id) + 1;
    $upvotes_users = Brand\get_upvotes_users($post_id);
  }
  else {
    $upvotes = Product\get_upvotes($post_id) + 1;
    $upvotes_users = Product\get_upvotes_users($post_id);
  }

  if($upvotes_users && is_array($upvotes_users)) {
    array_push($upvotes_users, $user_id);
  }
  else {
    $upvotes_users = array($user_id);
  }

  update_post_meta($post_id, $meta_key_prefix . '_upvotes', $upvotes);
  update_post_meta($post_id, $meta_key_prefix . '_upvotes_users', $upvotes_users);

  return $upvotes;
}

/**
 * Downvote
 */
function downvote($post_id, $user_id, $post_type, $meta_key_prefix) {
  if($post_type == 'mgl_brand') {
    $upvotes = Brand\get_upvotes($post_id) - 1;
    $upvotes_users = Brand\get_upvotes_users($post_id);
  }
  else {
    $upvotes = Product\get_upvotes($post_id) - 1;
    $upvotes_users = Product\get_upvotes_users($post_id);
  }

  if($upvotes_users && is_array($upvotes_users)) {
    $key = array_search($user_id, $upvotes_users);
    array_splice($upvotes_users, $key, 1);
  }

  update_post_meta($post_id, $meta_key_prefix . '_upvotes', $upvotes);
  update_post_meta($post_id, $meta_key_prefix . '_upvotes_users', $upvotes_users);

  return $upvotes;
}

/**
 * Get the value of the "slug" field
 *
 * @param array $object Details of current post.
 * @param string $field_name Name of field.
 * @param WP_REST_Request $request Current request
 *
 * @return mixed
 */
function get_miigle_slug( $object, $field_name, $request ) {
	return get_post_meta( $object['id'], $field_name, true );
}

/**
 * Get the value of the "slug" checkbox field
 *
 * @param array $object Details of current post.
 * @param string $field_name Name of field.
 * @param WP_REST_Request $request Current request
 *
 * @return mixed
 */
function get_miigle_slug_checkbox( $object, $field_name, $request ) {
	if ( get_post_meta( $object['id'], $field_name, true ) ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Handler for updating custom field data.
 *
 * @since 0.1.0
 *
 * @param mixed $value The value of the field
 * @param object $object The object from the response
 * @param string $field_name Name of field
 *
 * @return bool|int
 */
function update_miigle_slug( $value, $object, $field_name ) {
	if ( ! $value || ! is_string( $value ) ) {
		return 'Missing value';
	}

	return update_post_meta( $object->ID, $field_name, strip_tags( $value ) );
}

/**
 * Get the value of the "slug" field
 *
 * @param array $object Details of current post.
 * @param string $field_name Name of field.
 * @param WP_REST_Request $request Current request
 *
 * @return mixed
 */
function get_miigle_user_slug( $object, $field_name, $request ) {
	return get_user_meta( $object['id'], $field_name, true );
}

/**
 * Handler for updating custom field data.
 *
 * @since 0.1.0
 *
 * @param mixed $value The value of the field
 * @param object $object The object from the response
 * @param string $field_name Name of field
 *
 * @return bool|int
 */
function update_miigle_user_slug( $value, $object, $field_name ) {
	return update_user_meta( $object->ID, $field_name, strip_tags( $value ) );
}