<?php

namespace Miigle\Controllers\User;

use Miigle\Models\User;

/**
 * Creates a user
 */
function post( $data ) {
	if ( email_exists( $data['email'] ) ) {
		return get_user_by( 'email', $data['email'] )->ID;
	}
	$random_password = wp_generate_password( $length = 12, $include_standard_special_chars = false );
	$user_id         = wp_create_user( $data['email'], $random_password, $data['email'] );

	update_user_meta( $user_id, '_mgl_user_title', $data['job'] );
	update_user_meta( $user_id, '_mgl_user_company', $data['brandName'] );

	return wp_update_user( array(
		'ID'         => $user_id,
		'first_name' => $data['firstName'],
		'last_name'  => $data['lastName']
	) );
}

/**
 * Update a user
 */
function put( $data ) {
	global $wpdb;
	$user = wp_get_current_user();

	update_user_meta( $user->ID, '_mgl_user_title', $data['_mgl_user_title'] );
	update_user_meta( $user->ID, '_mgl_user_username', $data['_mgl_user_username'] );
	update_user_meta( $user->ID, '_mgl_user_website', $data['_mgl_user_website'] );
	update_user_meta( $user->ID, '_mgl_user_facebook', $data['_mgl_user_facebook'] );
	update_user_meta( $user->ID, '_mgl_user_twitter', $data['_mgl_user_twitter'] );
	update_user_meta( $user->ID, '_mgl_user_company', $data['_mgl_user_company'] );

	return wp_update_user( array(
		'ID'         => $user->ID,
		'user_email' => $data['email'],
		'first_name' => $data['first_name'],
		'last_name'  => $data['last_name']
	) );
}

/**
 * @return array Json of user activity
 */
function activity() {
	$user = wp_get_current_user();

	return User\activity( $user );
}

/**
 * Send an invite mails to user specified
 *
 * @param $data
 *
 * @return bool
 */
function invite( $data ) {
	$user = wp_get_current_user();

	$headers[] = "From: Miigle <notifications@{$_SERVER['SERVER_NAME']}>";
	return wp_mail( $data['to'], 'Check out Miigle.com', "Hey! {$user->display_name} would like you to check out " . get_home_url(), $headers );
}