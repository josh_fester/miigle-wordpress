<?php

namespace Miigle\Rest;

use Miigle\Controllers\Application;
use Miigle\Controllers\Product;
use Miigle\Controllers\Brand;
use Miigle\Controllers\User;
use Miigle\Controllers\Subscription;

function get_posts() {

    // The $_REQUEST contains all the data sent via ajax
    if ( isset($_REQUEST) ) {

        $fruit = $_REQUEST['fruit'];

        // Let's take the data that was sent and do something with it
        if ( $fruit == 'Banana' ) {
            $fruit = 'Apple';
        }

        // Now we'll return it to the javascript function
        // Anything outputted will be returned in the response
        echo $fruit;

        // If you're debugging, it might be useful to see what was sent in the $_REQUEST
        // print_r($_REQUEST);

    }

    // Always die in functions echoing ajax content
    die();
}

add_action( 'wp_ajax_nopriv_get_posts', 'get_posts' );

add_action( 'rest_api_init', __NAMESPACE__ . '\\register' );

function register() {

	register_rest_route( 'mgl/v1', '/product', array(
		'methods'             => 'POST',
		'callback'            => 'Miigle\\Controllers\\Product\\post',
		'permission_callback' => function () {
			return current_user_can( 'publish_mgl_products' );
		}
	) );

	register_rest_route( 'mgl/v1', '/product/upvote', array(
		'methods'             => 'POST',
		'callback'            => 'Miigle\\Controllers\\Product\\upvote_post',
		'permission_callback' => function () {
			return is_user_logged_in();
		}
	) );

	register_rest_route( 'mgl/v1', '/product/downvote', array(
		'methods'             => 'POST',
		'callback'            => 'Miigle\\Controllers\\Product\\downvote_post',
		'permission_callback' => function () {
			return is_user_logged_in();
		}
	) );

    register_rest_route( 'mgl/v1', '/product/posts_api', array(
        'methods'             => 'GET',
        'callback'            => 'Miigle\\Controllers\\Product\\posts_api',
        'permission_callback' => function () {
            return true;
        }
    ) );

    register_rest_route( 'mgl/v1', '/user', array(
		'methods'             => 'PUT',
		'callback'            => 'Miigle\\Controllers\\User\\put',
		'permission_callback' => function () {
			return is_user_logged_in();
		}
	) );

	register_rest_route( 'mgl/v1', '/user/new', array(
		'methods'             => 'POST',
		'callback'            => 'Miigle\\Controllers\\User\\post',
		'permission_callback' => function () {
			return true;
		}
	) );

	register_rest_route( 'mgl/v1', '/user/activity', array(
		'methods'             => 'GET',
		'callback'            => 'Miigle\\Controllers\\User\\activity',
		'permission_callback' => function () {
			return is_user_logged_in();
		}
	) );

	register_rest_route( 'mgl/v1', '/user/invite', array(
		'methods'             => 'POST',
		'callback'            => 'Miigle\\Controllers\\User\\invite',
		'permission_callback' => function () {
			return is_user_logged_in();
		},
		'args'                => array(
			'to' => array(
				'required'          => true,
				'validate_callback' => function ( $param, $request, $key ) {
					return filter_var( $param, FILTER_VALIDATE_EMAIL );
				},
				'sanitize_callback' => function ( $param, $request, $key ) {
					return filter_var( $param, FILTER_SANITIZE_EMAIL );
				},
				'description'       => 'Email address.',
			),
		),
	) );

	register_rest_route( 'mgl/v1', '/subscription', array(
		'methods'             => 'GET',
		'callback'            => 'Miigle\\Controllers\\Subscription\\index',
		'permission_callback' => function () {
			return true;
		}
	) );

	register_rest_route( 'mgl/v1', '/subscription/new', array(
		'methods'             => 'POST',
		'callback'            => 'Miigle\\Controllers\\Subscription\\post',
		'permission_callback' => function () {
			return true;
		}
	) );

	register_rest_route( 'mgl/v1', '/subscription/discount/(?P<code>.+)', array(
		'methods'             => 'GET',
		'callback'            => 'Miigle\\Controllers\\Subscription\\discount',
		'args'                => array(
			'code' => array(
				'required'          => true,
				'validate_callback' => function ( $param, $request, $key ) {
					return ctype_alnum( $param );
				},
				'sanitize_callback' => function ( $param, $request, $key ) {
					return filter_var( $param, FILTER_SANITIZE_STRING );
				},
			),
		),
		'permission_callback' => function () {
			return true;
		}
	) );

	register_rest_route( 'mgl/v1', '/brands', array(
		'methods'  => 'GET',
		'callback' => 'Miigle\\Controllers\\Brand\\filter',
		'args'     => array(
			'from' => array(
				'required'          => true,
				'validate_callback' => function ( $param, $request, $key ) {
					return preg_match( '/^(.)$/', $param );
				}
			),
			'to'   => array(
				'required'          => true,
				'validate_callback' => function ( $param, $request, $key ) {
					return preg_match( '/^(.)$/', $param );
				}
			),
		)
	) );

	register_rest_route( 'mgl/v1', 'brand/(?P<id>\d+)/thumbs', array(
		'methods'  => 'GET',
		'callback' => 'Miigle\\Controllers\\Brand\\get_brand_thumbnails',
		'args'     => array(
			'id'    => array(
				'required'          => true,
				'validate_callback' => function ( $param, $request, $key ) {
					return is_numeric( $param );
				},
				'sanitize_callback' => function ( $param, $request, $key ) {
					return intval( $param );
				},
				'description'       => 'ID of the brand.',
			),
			'count' => array(
				'required'          => false,
				'default'           => 4,
				'validate_callback' => function ( $param, $request, $key ) {
					return is_numeric( $param );
				},
				'sanitize_callback' => function ( $param, $request, $key ) {
					return intval( $param );
				},
				'description'       => 'Number of thumbnails to return.',
			)
		)
	) );
}
