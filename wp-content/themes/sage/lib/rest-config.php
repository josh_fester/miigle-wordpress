<?php

namespace Miigle\RestConfig;

add_filter( 'rest_mgl_brand_collection_params', __NAMESPACE__ . '\\add_orderby_param', 10, 2 );
add_filter( 'rest_mgl_product_collection_params', __NAMESPACE__ . '\\add_orderby_param', 10, 2 );
function add_orderby_param( $params, $post_type_obj ) {
	$params['orderby'] = array(
		'description' => __( 'Sort collection by object attribute.' ),
		'type'        => 'string',
		'default'     => 'date',
		'enum'        => array(
			'date',
			'relevance',
			'id',
			'include',
			'title',
			'slug',
			'rand',
			'_mgl_brand_is_sponsored',
		),
	);

	return $params;
}

add_filter( 'rest_mgl_brand_query', __NAMESPACE__ . '\\default_brand_filter', 20, 2 );
function default_brand_filter( $args, $request ) {
	$args['meta_query'][] = array(
		'key'   => '_mgl_brand_is_paid',
		'value' => 'on',
	);

	return $args;
}

add_filter( 'rest_mgl_brand_query', __NAMESPACE__ . '\\add_filter_query', 10, 2 );
add_filter( 'rest_mgl_product_query', __NAMESPACE__ . '\\add_filter_query', 10, 2 );
function add_filter_query( $args, $request ) {

	if ( isset( $request['filter'] ) ) {
		$args = array_merge( $args, $request['filter'] );
	}

	if ( isset( $request['from'] ) && isset( $request['to'] ) ) {
		global $wpdb;

		$postids = $wpdb->get_col( $wpdb->prepare( "
			SELECT      ID
			FROM        $wpdb->posts
			WHERE       SUBSTR($wpdb->posts.post_title,1,1) >= %s
			AND         SUBSTR($wpdb->posts.post_title,1,1) <= %s
			AND 		$wpdb->posts.post_type = 'mgl_brand'
			ORDER BY    $wpdb->posts.post_title", $request['from'], $request['to'] ) );

		$args['post__in'] = $postids;
	}

	return $args;
}

add_filter( 'posts_results', __NAMESPACE__ . '\\order_by_featured', PHP_INT_MAX, 2 );
function order_by_featured( $posts, $query ) {
	if ( $query->get( 'post_type' ) === 'mgl_brand' ) {
		// run once
		remove_filter( current_filter(), PHP_INT_MAX, 2 );
		$nonsponsored = array();
		$sponsored    = array();
		foreach ( $posts as $post ) {
			if ( get_post_meta( $post->ID, '_mgl_brand_is_sponsored', true ) ) {
				$sponsored[] = $post;
			} else {
				$nonsponsored[] = $post;
			}
		}

		// if sponsored are always at top
		$posts = array_merge( $sponsored, $nonsponsored );
	}

	return $posts;
}