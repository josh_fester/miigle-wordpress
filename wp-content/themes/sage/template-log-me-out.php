<?php
  if (is_user_logged_in()) {
    wp_logout();
    write_log( 'User logged out!' );
  }
?>
