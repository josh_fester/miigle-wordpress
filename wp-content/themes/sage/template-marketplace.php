<?php
/**
 * Template Name: Marketplace
 */
?>

<div id="template-marketplace">

	<section id="marketplace">
		<div class="container">
          <!-- Beginning of header content -->
          <div class="row">
            <div class="col-xs-12">
                <img id="logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/white-logo-two.svg" >
                <?php the_content(); ?>
            </div>
          </div>
          <!-- End of header content -->

          <!-- Beginning of main buttons 
           <div class="row"> 
              <div class="col-xs-12">
                  <div class="btn btn-prime-two">
                      <a href="<?= home_url() ?>">GO TO HOMEPAGE</a>
                  </div>

                  <div class="btn btn-prime-one ">
                      <a href="<?= home_url() ?>/brands">VIEW DIRECTORY</a>
                  </div>
              </div>
           </div>
           End of main buttons -->

    </div>
	</section>

</div>