<?php
/**
 * Template Name: Product Page - Marketplace
 */

use Miigle\Models\Product;
use Miigle\Models\Brand;

$gallery = Product\get_image_gallery(['id' => $post->ID], '_mgl_product_image_gallery', '');
$categories = Product\get_categories(get_the_ID());
?>
<style>
    .the_champ_horizontal_sharing .theChampSharingRound {
        width: 124px !important;
        margin-right: 13px !important;
    }

    .the_champ_horizontal_sharing .theChampSharingRound > i {
        width: 124px !important;
        border-radius: 4px !important;
        border: none !important;
    }
</style>
<div id="template-product_page" class="v2">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <section id="product_page">
        <div class="container">
            <div class="row">
                <div class="col-md-5 prod-gallery hidden-xs hidden-sm">

                    <div class="row"><!-- Large View -->
                        <div class="col-xs-12">
                            <div class="full">
                                <img id="image-preview" src="<?php echo Product\get_thumbnail(get_the_ID()) ?>" class="img-responsive"/>
                            </div>
                        </div>
                    </div>
                    <div class="row"><!-- Thumbnail View -->

                       <?php $i = 0; ?>
                            <?php foreach ($gallery as $k => $v): ?>
                                <?php if($i < 3): ?>
						<div class="col-nano-3 previews">
                                    <a href="#" class="image" <?php if ($k == 0): ?>class="selected"<?php endif; ?>
                                       data-full="<?= $v['image'] ?>">
                                        <img src="<?= $v['image'] ?>" class="img-responsive"/>
                                    </a>
                                </div>
                            <?php endif; ?>
                            <?php $i++ ?>
                        <?php endforeach; ?>

                        <!-- MAX IMG 4 -->
                    </div>

                </div>
				<?php $brand = Product\get_brand(get_the_ID()); ?> 
                <div class="col-md-7 prod-data">
                    <div class="prod--info text-upper">
                        <h3>
							<a href="<?php echo get_the_permalink($brand->ID) ?>"><?php echo Product\get_brand_name(get_the_ID()) ?></a>
                        </h3>
                        <div class="mFlex">
                            <h1>
                                <?php the_title() ?>
                            </h1>
                            <p class="prod-price">
                                <?php echo Product\get_price(get_the_ID()) ?>
                            </p>
                        </div>
                    </div>
                    <div class="prod--category pb3">
                        <?php foreach (Product\get_categories(get_the_ID()) as $category): ?>
                            <?php if (!$category->parent): ?>
                                <a href="<?= home_url() ?>/marketplace?category=<?= $category->name ?>"
                                   class="btn btn-category btn-<?= $category->name ?>">  <?= $category->name ?> </a>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <!-- .prod-gallery Show ONLY MOBILE -->
                    <div class="prod-gallery visible-xs visible-sm pb3">
                        <div class="row"><!-- Large View -->
                            <div class="col-xs-12">
                                <div class="full">

                                    <img id="image-preview" src="<?php echo Product\get_thumbnail(get_the_ID()) ?>" class="img-responsive"/>

                                </div>
                            </div>
                        </div>
                        <div class="row"><!-- Thumbnail View -->
                            <div class="col-nano-3 previews">

                                <?php $i = 0; ?>
                                <?php foreach ($gallery as $k => $v): ?>
                                <?php if($i < 3): ?>
                                    <div class="col-nano-3 previews">
                                        <a href="#" class="image" <?php if ($k == 0): ?>class="selected"<?php endif; ?>
                                           data-full="<?= $v['image'] ?>">
                                            <img src="<?= $v['image'] ?>" class="img-responsive"/>
                                        </a>
                                    </div>
                                    <?php endif; ?>
                                    <?php $i++ ?>
                                <?php endforeach; ?>

                                <!-- MAX IMG 4 -->
                            </div>

                        </div>
                    </div>
                    <!-- END.prod-gallery  -->
                    <div class="prod--description pb3">
                        <?php 
							$getPost = get_the_content();
							$postwithbreaks = wpautop( $getPost, true );
							$withAdditional = apply_filters('the_content', $postwithbreaks);
							echo $withAdditional;
						?>
                    </div>
                    <div class="prod--action">
                        <a href="<?php echo Product\get_url(get_the_ID()) ?>" class="btn btn-action btn-buy text-upper">Buy</a>
                        <!--<a href="#share" class="btn btn-action btn-share text-upper">Share</a>-->
                        <!--<a href="#like" class="btn btn-action btn-like text-upper">Like</a>-->

                        <div class="prod--share">
                            <?= do_shortcode('[TheChamp-Sharing]'); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="section--search-bar" class="pt5">
        <?php get_template_part('templates/include', 'search'); ?>
    </section>

    <section id="section--similar-products" class="section--pad">
        <div class="container">
            <!-- TITLE ROW -->
            <div class="row">
                <div class="col-nano-12">
                    <div class="title--pad">
                        <h2 class="background"><span>Similar Products</span></h2>
                    </div>
                </div>
            </div>

            <div id="posts-container" class="row masonry-container">

                <div class="col-nano-12 col-micro-6 col-xs-6 col-sm-6 col-md-4 col-lg-3 masonry-sizer"></div>
                <?php
                wp_reset_postdata();
                $filters = '';
                foreach ($categories as $category) {

                    $filters .= $category->slug . ',';
                }
                $filters = substr($filters, 0, -1);

                $the_query = new WP_Query(['post_type' => 'mgl_product', 'mgl_product_category' => $filters, 'orderby' => 'rand', 'post_status' => 'published']);

                if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
                    ?>

                    <div class="col-lg-3 col-md-4 col-nano-6 masonry-item">
                        <div class="thumbnail">
                            <img src="<?php echo Product\get_thumbnail(get_the_ID()) ?>" class="">
                            <div class="caption">
                                <p class="brand">
                                    <?php echo Product\get_brand_title(get_the_ID()) ?>

                                </p>

                                <h3><?php the_title() ?></h3>
                                <p class="price"><?= Product\get_price(get_the_ID()) ?></p>
                            </div>
                            <a href="<?php echo get_the_permalink() ?>">
                                <div class="mask"></div>
                            </a>
                        </div>
                    </div>
                <?php endwhile; ?>
                <?php endif;
                wp_reset_postdata(); ?>

            </div>

            <div class="row view-more" id="viewProducts">
                <div class="col-nano-12 text-center">
                    <a class="btn-view-more text-upper" id="load-similar">View More</a>
                </div>
            </div>

        </div>
    </section>
<?php endwhile; endif; ?>
</div>

<script type="text/javascript" >


    jQuery(document).ready(function() {

        var imageContainer = document.getElementById('image-preview');
       	jQuery('.image').on('click', changeImage);

		function changeImage(){
			var imageURL = jQuery(this).data('full');
			jQuery(imageContainer).attr('src', imageURL);
		}
        var productID = <?php echo get_the_ID() ?> ;

        jQuery("#load-similar").on('click', function (e) {
            getSimilarProducts();
        });

        function getSimilarProducts(){

            /**
             * Ajax call
             */

            jQuery.when(
                jQuery.ajax({
                    url: '/getSimilarProducts.php?&productID="' + productID + '"',
                    success: function(response){
                        var data = jQuery.parseJSON(response);
                        // container selector
                        var container = jQuery("#posts-container");

                        // Displays data with an .each loop
                        jQuery.each(data, function (key, product)
                        {
                            container.append('<div class="col-lg-3 col-md-4 col-nano-6 masonry-item">'+
                                '<div class="thumbnail">'+
                                '<img src="' + product.thumbnail + '" class="">'+
                                '<div class="caption">'+
                                '<p class="brand">'+
                                product.brand +
                                '</p>'+

                                '<h3>' +
                                product.title +
                                '</h3>' +
                                '<p class="price">' +
                                product.price +
                                '</p>'+
                                '</div>' +
                                '<a href="' + product.url + '"><div class="mask"></div></a>' +
                                '</div>' +
                                '</div>');
                        });


                    }
                })
            ).then(masonry2);


        }

        var first = true;
        masonry2();
        first = false;
        function masonry2(){
            var masonry = jQuery('#posts-container');

            jQuery(masonry).imagesLoaded( function() {
                jQuery(masonry).masonry({
                    columnWidth: '.masonry-sizer',
                    itemSelector: '.masonry-item',
                    percentPosition: true
                });
            });
            if (!first)
            {
                jQuery(masonry).masonry('reload');
                jQuery(masonry).masonry('layout');
            }

        }

    });


</script>




