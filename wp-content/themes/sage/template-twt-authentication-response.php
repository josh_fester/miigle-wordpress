<?php
if(!session_id()) {
  session_start();
}

echo '<script>document.title = "Miigle+ | Authenticate with Twitter"</script>';

// include Twitter PHP SDK (used version which supports PHP 5.4!)
require_once(dirname(__FILE__) . '/vendor/Twitter/autoload.php');
use Abraham\TwitterOAuth\TwitterOAuth;


if ( !empty( $_GET['callback'] ) ) { // expects callback url to be present
  write_log( 'Twitter Authentication successfull so far, redirect back successfull, processing access token now.' );
  $callback_url = urldecode($_GET['callback']);
  $chrome_extension_part_pos = strpos($callback_url, 'chrome-extension://');
  $callback_file_part_pos = strpos($callback_url, 'oauth2_callback.html');

  if ( $chrome_extension_part_pos !== false && $callback_file_part_pos !== false ) { // callback url looks valid, continue with authentication
  
    $request_token = [];
    $request_token['twt_oauth_token'] = $_SESSION['twt_oauth_token'];
    $request_token['twt_oauth_token_secret'] = $_SESSION['twt_oauth_token_secret'];

    if (isset($_REQUEST['oauth_token']) && $request_token['twt_oauth_token'] !== $_REQUEST['oauth_token']) {
      echo 'ERROR (#8): Authentication through Twitter failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
      write_log( 'ERROR (#8): Authentication token not present in session' );
      exit;
    }

    try {
      $connection = new TwitterOAuth('YoNqI0IgE9qikpufhxj5V9Aka', 'KMvZRVBU0kHFeZv3cPalW91ad98e5hms5i1yXSEbFhwgK73Fkh', $request_token['twt_oauth_token'], $request_token['twt_oauth_token_secret']);

      $accessToken = $connection->oauth("oauth/access_token", ["oauth_verifier" => $_REQUEST['oauth_verifier']]);

      if (!isset($accessToken)) {
        echo 'ERROR (#9): Authentication through Twitter failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
        write_log( 'ERROR (#9): Access token not available' );
        exit;
      }

      $_SESSION['twt_oauth_token'] = $accessToken['oauth_token'];
      $_SESSION['twt_oauth_token_secret'] = $accessToken['oauth_token_secret'];

      $connection = new TwitterOAuth('YoNqI0IgE9qikpufhxj5V9Aka', 'KMvZRVBU0kHFeZv3cPalW91ad98e5hms5i1yXSEbFhwgK73Fkh', $accessToken['oauth_token'], $accessToken['oauth_token_secret']);

      // Proceed knowing you have a logged in user who's authenticated.
      $user_twitter_profile_data = $connection->get("account/verify_credentials", array('include_email' => 'true'));
      $user_twitter_email = $user_twitter_profile_data->email;
    } catch (TwitterOAuthException $e) {
      echo 'ERROR (#10): Authentication through Twitter failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
      write_log( 'ERROR (#10): ' . $e->getMessage() );
      exit;
    }

    if ( empty( $user_twitter_profile_data->id ) || empty( $user_twitter_profile_data->name ) ) {
      echo 'ERROR (#11): Authentication through Twitter failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
      write_log( 'ERROR (#11): Twitter User ID or Name not present.' );
      exit;
    }

    $user_twitter_name_first_last = explode(' ', $user_twitter_profile_data->name);

    $user_twitter_profile_image = '';

    if ( !empty( $user_twitter_profile_data->profile_image_url ) ) {
      $user_twitter_profile_image = str_replace('_normal', '', $user_twitter_profile_data->profile_image_url);
    }

    write_log( 'Everything fine with Twitter authentication, moving to WordPress authentication' );

    if (is_user_logged_in()) {
      $current_user = wp_get_current_user();
      write_log( 'User already logged in in WordPress (' . $current_user->ID . ', ' . $current_user->user_login . '), logging out to prevent wrong account linking!' );
      wp_logout();
    }

    if (empty($user_twitter_email)) {
      write_log( 'No twitter email for this user, so create a dummy one.' );
      $user_twitter_email = str_replace(' ', '_', strtolower(sanitize_user($user_twitter_profile_data->name))) . '@twitter.com';
    }

    $ID = email_exists($user_twitter_email);

    if ($ID == false) { // user with this email does not exist
      // require_once (ABSPATH . WPINC . '/registration.php');
      write_log( 'User with this email also does not exist' );
      $random_password = wp_generate_password($length = 12, $include_standard_special_chars = false);
      $sanitized_user_login = str_replace(' ', '_', strtolower(sanitize_user($user_twitter_profile_data->name)));

      if (!validate_username($sanitized_user_login)) {
        $sanitized_user_login = sanitize_user('twitter_' . $user_twitter_profile_data->id);
      }

      $default_user_name = $sanitized_user_login;
      $i = 1;

      while (username_exists($sanitized_user_login)) {
        $sanitized_user_login = $default_user_name . $i;
        $i++;
      }

      write_log( 'Creating new user:' . $sanitized_user_login . '(' . $user_twitter_email . ')' );
      $ID = wp_create_user($sanitized_user_login, $random_password, $user_twitter_email);

      if (!is_wp_error($ID)) {
        wp_new_user_notification($ID);
        $user_info = get_userdata($ID);
        wp_update_user(array(
          'ID' => $ID,
          'display_name' => $user_twitter_profile_data->name,
          'first_name' => $user_twitter_name_first_last[0],
          'last_name' => $user_twitter_name_first_last[1],
          'twitter' => 'https://twitter.com/' . $user_twitter_profile_data->screen_name
        ));
      } else {
        echo 'ERROR (#12): Authentication through Twitter failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
        write_log( 'ERROR (#12):' . $ID->get_error_message() );
        exit;
      }
    } else { // user with this email already exists
      write_log( 'User with this email (' . $user_twitter_email . ') already exists, new account was not created!' );
    }

    // login
    $secure_cookie = is_ssl();
    $secure_cookie = apply_filters('secure_signon_cookie', $secure_cookie, array());
    global $auth_secure_cookie; // XXX ugly hack to pass this to wp_authenticate_cookie
    $auth_secure_cookie = $secure_cookie;
    wp_set_auth_cookie($ID, true, $secure_cookie);
    $user_info = get_userdata($ID);
    do_action('wp_login', $user_info->user_login, $user_info);
    update_user_meta($ID, 'thechamp_avatar', $user_twitter_profile_image);
    update_user_meta($ID, 'thechamp_large_avatar', $user_twitter_profile_image);

    write_log( 'Twitter authentication for ' . $user_twitter_profile_data->name . ' (ID: ' . $user_twitter_profile_data->id . '), WordPress authentication (ID: ' . $ID . ') all done, now querying for WordPress authentication token.' );

    try {
      // get access token for wp oauth
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,"https://miigle.com/oauth/token");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
        'client_id' => 'VkdpA09M4633hGIQMYG89PPAk5GQmvUiQ7Iq8Rcc',
        'client_secret' => 'YfffWY6JXMR7CGHWMoN9UHrKP9aX1m9ZCpfynmhl',
        'grant_type' => 'client_credentials'
      )));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $server_output = curl_exec ($ch);
      curl_close ($ch);
      // further processing ....
      $server_output = json_decode($server_output);
    } catch (Exception $e) {
      echo 'ERROR (#13): Authentication through Twitter failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
      write_log( 'ERROR (#13):' . $e->getMessage() );
      exit;
    }

    // try {
    //   // get access token for wp oauth
    //   $ch_2 = curl_init();
    //   curl_setopt($ch_2, CURLOPT_URL,"https://www.miigle.com/wp-json/wp/v2/users/".$ID);
    //   $headr = array();
    //   $headr[] = 'Authorization: Bearer '.$server_output->access_token;
    //   curl_setopt($ch_2, CURLOPT_HTTPHEADER,$headr);
    //   curl_setopt($ch_2, CURLOPT_RETURNTRANSFER, true);
    //   $server_output_2 = curl_exec ($ch_2);
    //   curl_close ($ch_2);
    //   // further processing ....
    //   $server_output_2 = json_decode($server_output_2);
    //   write_log( 'Output of the test of token ' . $server_output->access_token . ', on endpoint ' . 'https://www.miigle.com/wp-json/wp/v2/users/' . $ID );
    //   write_log( print_r($server_output_2, true) );
    // } catch (Exception $e) {
    //   echo 'ERROR (#14): Authentication through Twitter failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
    //   write_log( 'ERROR (#14):' . $e->getMessage() );
    //   exit;
    // }

    global $wpdb;
    $is_updated = $wpdb->update( 'wp_oauth_access_tokens', array( 'user_id' => $ID ), array( 'access_token' => $server_output->access_token ), array( '%d' ), array( '%s' ) );
    
    write_log( 'update for access token returned: ' . $is_updated );
    write_log( 'All successfully done, doing the redirect now to ' . urldecode( $callback_url ) . '?access_token=' . $server_output->access_token . '&ID=' . $ID . '.' );

    if (!empty($_GET['debug'])) {
      header('Location: ' . get_edit_profile_url());
    } else {
      header('Location: ' . urldecode( $callback_url ) . '?access_token=' . $server_output->access_token . '&ID=' . $ID );
    }

  } else if ( $chrome_extension_part_pos === false ) { // callback not to chrome extension, terminate
    write_log( 'ERROR (#6): callback URL "' . $callback_url . '" is not a valid one, chrome extension protocol not present.' );
    echo 'ERROR (#6): Wrong callback.';
    die();
    exit;
  } else if ( $callback_file_part_pos === false ) { // callback not to oauth2_callback.html page, terminate
    write_log( 'ERROR (#7): callback URL "' . $callback_url . '" is not a valid one, callback page is not the expected one.' );
    echo 'ERROR (#7): Wrong callback.';
    die();
    exit;
  }
} else {
  write_log( 'ERROR (#5): callback URL is missing from the twitter authentication request.' );
  echo 'ERROR (#5): Wrong callback.';
  die();
  exit;
}
?>
