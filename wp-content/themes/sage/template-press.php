<?php
/**
 * Template Name: Press Page
 */
?>

<div class="press-nav-container">
  <div class="press-tabs w-tabs" data-duration-in="400" data-duration-out="200" data-easing="ease-in">
    <div class="press-tabs-menu w-tab-menu">
      <a class="press-tab-link w--current w-inline-block w-tab-link" data-w-tab="Tab 1">
        <div class="press-toggle-link"><?php the_field('tab_1_label'); ?></div>
      </a>
      <a class="press-tab-link w-inline-block w-tab-link" data-w-tab="Tab 2">
        <div class="press-toggle-link"><?php the_field('tab_2_label'); ?></div>
      </a>
      <a class="press-tab-link w-inline-block w-tab-link" data-w-tab="Tab 3">
        <div class="press-toggle-link"><?php the_field('tab_3_label'); ?></div>
      </a>
    </div>
    <div class="press-tabs-content w-tab-content">
      <div class="press-tab w--tab-active w-tab-pane" data-w-tab="Tab 1">
        <?php 
          $args = array(
            'posts_per_page'   => -1,
            'offset'           => 0,
            'category'         => '',
            'category_name'    => 'press',
            'orderby'          => 'date',
            'order'            => 'DESC',
            'post_type'        => 'post',
            'post_status'      => 'publish',
            'suppress_filters' => true 
          );
          $press_posts_array = get_posts( $args ); 
          foreach ( $press_posts_array as $post ) : setup_postdata( $post ); ?> 
          
            <div class="press-card">
              <p class="press-date"><?php the_date(); ?></p>
              <h3 class="press-title"><?php the_title(); ?></h3>
              <p class="paragraph"><?php the_excerpt(); ?></p>
              <a class="press-read-more-link" href="<?php the_permalink(); ?>">Read more</a>
            </div>

          <?php endforeach; 
          wp_reset_postdata();
        ?>

        <div class="view-more-container">
          <!-- <a class="view-more-btn w-button" href="#">VIEW MORE</a> -->
        </div>
      </div>
      <div class="press-tab w-tab-pane" data-w-tab="Tab 2">
        <div class="our-story-div">
          <?php the_field('tab_2_content'); ?>
          <div class="about-us-bar">
            <p class="about-us-p">Want to learn more about our mission, team and CSR? Visit our <a href="/about" class="visit-about-us"><span class="about-us-link">ABOUT</span></a> page.</p>
          </div>
        </div>
        <div class="press-media-container">
          <p class="paragraph">Media inquiry? No worries, <a href="mailto:Hello@Miigle.com?subject=Media Inquiry" class="press-contact-link">send us an email</a>.</p>
        </div>
      </div>
      <div class="press-tab w-tab-pane" data-w-tab="Tab 3">
        <?php
          $boxes = get_field('tab_3_boxes');
          if ( !empty( $boxes ) && count( $boxes ) > 0 ) { ?>
            <div class="press-assets-row">
            <?php
            foreach ($boxes as $key => $box) { ?>
              <div class="press-assets-col">
                <img class="press-assets-img" height="70" src="<?php echo $box['tab_3_box_icon_image']; ?>">
                <a class="press-cta-btn w-button" href="<?php echo $box['tab_3_box_button_link']; ?>" target="_blank"><?php echo $box['tab_3_box_button_text']; ?></a>
              </div>
            <?php } ?>
            </div>
            <?php
          }
        ?>
          <div class="press-media-container">
            <p class="paragraph">Media inquiry? No worries, <a href=" mailto:Hello@Miigle.com?subject=Media Inquiry" class="press-contact-link">send us an email</a>.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
