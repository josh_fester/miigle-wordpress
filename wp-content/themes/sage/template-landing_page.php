<?php
/**
 * Template Name: Landing Page
 */
?>
<style>
@media only screen and (min-width: 1440px){
	.container {
    	width: 1140px;
	}
}
footer#footer .links {
    padding: 75px 0;
}
footer#footer h4 {
	font-size: 16px;
	letter-spacing: 2px;
	font-weight: 600;
}
footer#footer .links a {
    font-size: 15px;
	letter-spacing: 2px;
}
footer#footer p {
    font-size: 17px;
    font-weight: 400;
	color:#fff;
}
.newsletter-form .form-row--btn-right-inside {
	border: 1px solid #f5f6f6 !important;
	background-color: #f5f6f6 !important;
	border-radius: 30px;
    height: 50px;
	padding-right: 4px;
    transition: .2s ease-in-out;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
}
.newsletter-form .form-control {
	background-color: #f5f6f6 !important;
	height: 48px !important;
	font-size: 18px;
    display: block;
    width: 100%;
	padding: 0 15px;
	box-shadow: none;
}
.newsletter-form .btn {
	border:none !important;
	display: inline-block;
    overflow: hidden;
    margin: 0;
    padding: 0 15px;
    height: 40px;
    border-radius: 50px;
    background: linear-gradient(90deg,#55b8b8,#89d0bb);
    color: #fff;
    vertical-align: middle;
    text-align: center;
    text-decoration: none;
    text-transform: uppercase;
    font-family: inherit;
    text-overflow: ellipsis;
    white-space: nowrap;
    letter-spacing: inherit;
    font-size: 14px;
    font-weight: 400;
    line-height: 36px;
    outline: 0;
    transition: .2s ease-in-out;
    cursor: pointer;
    -webkit-font-smoothing: inherit;
}
.newsletter-form input::-webkit-input-placeholder {color:#2a414d; /* Webkit Browsers */}
.newsletter-form input:-moz-placeholder {color:#2a414d; /* Firefox 18- */}
.newsletter-form input::-moz-placeholder {color:#2a414d; /* Firefox 19+ */}
.newsletter-form input:-ms-input-placeholder {color:#2a414d; /* IE10 */}
.mb3 {
    margin-bottom: 30px;
}
.ml-auto {
    margin-left: auto!important;
}

/* QQ fix */
.featured-nav>ul>li>a {
  font-weight: 500;
}
section.lp-hero h1 {
  font-size: 32px;
}
section.lp-hero h3 {
  font-size: 20px;
}
section.lp-features h2,
section.lp-brands h2,
section.lp-join h2 {
  font-size: 32px;
}
section.lp-features .how-it-works p,
section.lp-features .lp-pricing p,
.testimonials-carousel p {
  font-size: 17px;  
}
  @media (min-width: 992px) {
    #mgl-primary-collapsed ul li:first-child, #mgl-primary-collapsed ul li:nth-child(2), #mgl-primary-collapsed ul li:nth-child(3) {
      display: none;
    }
  }

</style>
<?php if ( get_field( 'notification_enable', 'option' ) ) : ?>
    <div class="notice flex--align-vertical">
        <div class="container">
            <div class="row flex--align-vertical">
				<div class="col-xl-8 col-lg-8 col-sm-8 notification-text">
                    <div class="h5 mb0 mt0"><?php the_field( 'notification_text', 'option' ); ?></div>
                </div>

				<?php if ( $link = get_field( 'notification_link', 'option' ) ) : ?>
					<div class="col-xl-4 col-lg-4 col-sm-4 text-right notification-link">
                        <a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="btn btn--white"><?php echo $link['title']; ?></a>
                    </div>
				<?php endif; ?>
            </div>
        </div>

        <button class="notice__close icon-close"></button>
    </div>
<?php endif; ?>

<?php while ( have_rows( 'lp-flex-content' ) ): the_row(); ?>

	<?php if ( get_row_layout() == 'signup_section' ) : ?>
        <section class="lp-hero background-gray-lighter background-gray-lighter--pattern pt150 pt50-sm-down mb100 mb50-sm-down">
            <div class="container mb150 mb50-sm-down">
                <div class="row flex--justify-center">
                    <div class="col-md-12 text-center">

						<?php if ( $vimeo = get_sub_field( 'video_url' ) ): ?>
                            <a href="#watch-video-signup" class="btn-play">
                                <img src="<?php echo get_template_directory_uri(); ?>/dist/images/icon-play-circle.svg"
                                     class="img-80" alt="Play Icon">
                                <p class="mb0 text-bold text-uppercase mt20">Watch Video</p>
                            </a>

                            <div class="remodal remodal--video" data-remodal-id="watch-video-signup">
                                <button data-remodal-action="close" class="remodal__close">
                                    <span class="icon-close"></span>
                                </button>

                                <div class="remodal__container remodal__container--video">
                                    <div class="js-video" data-type="vimeo"
                                         data-video-id="<?php echo substr( parse_url( $vimeo, PHP_URL_PATH ), 1 ); ?>"></div>
                                </div>
                            </div>
						<?php endif; ?>

                        <h1 class="text-medium text-color-gray-darkest mt50 mb20"><?php the_sub_field( 'title' ); ?></h1>
                    </div>

					<div class="col-md-8 text-center mb30">
						<h3 class="text-regular"><?php the_sub_field( 'subtitle' ); ?></h3>
					</div>

                    <div class="col-md-8 col-lg-6 text-center">
                        <a href="https://chrome.google.com/webstore/detail/miigle/gcecljemibcfdfmkheegndapbjjfealp" target="_blank" class="btn btn--large">Download extension</a>
                    </div>
                </div>
            </div>

            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/hero-cards-bg.png" class="element-block"
                 alt="Hero Cards">
        </section>
	<?php endif; ?>

	<?php if ( get_row_layout() == 'consumerbrand_section' ) : ?>
        <section class="lp-features mt100 mt50-sm-down mb100 mb50-sm-down pb100 pb50-sm-down border-bottom border-green-lightest">
			<?php if ( have_rows( 'tabs' ) ): ?>
                <div class="container">
                    <div class="tabs-bar mb100 mb50-sm-down">
                        <ul>
							<?php while ( have_rows( 'tabs' ) ) : the_row(); ?>
                                <li<?php echo get_row_index() == 1 ? ' class="active"' : ''; ?>>
                                    <a href="#<?php echo get_row_layout() . '-' . get_row_index(); ?>"><?php the_sub_field( 'tab_title' ); ?></a>
                                </li>
							<?php endwhile; ?>
                        </ul>
                    </div>
                </div>
			<?php endif; ?>

			<?php while ( have_rows( 'tabs' ) ) : the_row(); ?>
				<?php if ( get_row_layout() == 'consumer' ) : ?>
                    <div class="tabs-panel active" id="consumer-<?php the_row_index(); ?>">
                        <div class="mb100 mb50-sm-down pb100 pb50-sm-down border-bottom border-green-lightest">
                            <div class="container">
                                <h2 class="h1 text-center text-color-green mb100 mb50-sm-down"><?php the_sub_field( 'subtitle' ); ?></h2>

								<?php if ( $vimeo = get_sub_field( 'video_url' ) ): ?>
                                    <a href="#watch-video-<?php echo get_row_layout() . '-' . get_row_index(); ?>"
                                       class="how-it-works-video mb100"
                                       style="background-image: url('<?php echo get_template_directory_uri(); ?>/dist/images/how-it-works/how-it-works-video.png')">
                                        <button type="button" class="btn-play">
                                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/icon-play-circle.svg"
                                                 class="img-80" alt="Play Icon">
                                            <p class="mb0 text-bold text-uppercase mt20">Watch the 60 seconds animated
                                                explainer</p>
                                        </button>
                                    </a>

                                    <div class="remodal remodal--video-anim"
                                         data-remodal-id="watch-video-<?php echo get_row_layout() . '-' . get_row_index(); ?>">
                                        <button data-remodal-action="close" class="remodal__close">
                                            <span class="icon-close"></span>
                                        </button>

                                        <div class="remodal__container remodal__container--video">
                                            <div class="js-video" data-type="vimeo"
                                                 data-video-id="<?php echo substr( parse_url( $vimeo, PHP_URL_PATH ), 1 ); ?>"></div>
                                        </div>
                                    </div>
								<?php endif; ?>

                                <div class="how-it-works">
									<?php while ( have_rows( 'items' ) ) : the_row(); ?>
                                        <article class="how-it-works__item row">
                                            <div class="col-md-7">
												<?php echo wp_get_attachment_image( get_sub_field( 'image' ), 'large' ); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <p class="mb0"><?php the_sub_field( 'text' ); ?></p>
                                            </div>
                                        </article>
									<?php endwhile; ?>

									<?php if ( $download_extension_url = get_sub_field( 'download_extension_url' ) ) : ?>
                                        <div class="text-center">
                                            <a href="<?php echo $download_extension_url; ?>" target="_blank" class="btn btn--large">Download
                                                extension</a>
                                        </div>
									<?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="container lp-pricing">
                            <h2 class="h1 text-center text-color-green mb100 mb50-sm-down"><?php the_sub_field( 'pricing_title' ); ?></h2>

                            <div class="text-center">
                                <h4 class="text-regular mb30"><?php the_sub_field( 'pricing_subtitle' ); ?></h4>
                                <div class="h4 text-book mb100 mb50-sm-down"><?php the_sub_field( 'pricing_text' ); ?></div>

								<?php if ( $download_extension_url = get_sub_field( 'download_extension_url' ) ) : ?>
                                    <a href="<?php echo $download_extension_url; ?>" target="_blank" class="btn btn--large">Download
                                        extension</a>
								<?php endif; ?>
                            </div>
                        </div>
                    </div>
				<?php elseif ( get_row_layout() == 'brand' ): ?>
                    <div class="tabs-panel" id="brand-<?php the_row_index(); ?>">
                        <div class="mb100 mb50-sm-down border-bottom border-green-lightest">
                            <div class="container">
                                <h2 class="h1 text-center text-color-green mb100 mb50-sm-down"><?php the_sub_field( 'subtitle' ); ?></h2>

								<?php if ( $vimeo = get_sub_field( 'video_url' ) ): ?>
                                    <a href="#watch-video-<?php echo get_row_layout() . '-' . get_row_index(); ?>"
                                       class="how-it-works-video mb100"
                                       style="background-image: url('<?php echo get_template_directory_uri(); ?>/dist/images/how-it-works/how-it-works-video-2.png')">
                                        <button type="button" class="btn-play">
                                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/icon-play-circle.svg"
                                                 class="img-80" alt="Play Icon">
                                            <p class="mb0 text-bold text-uppercase mt20">Watch the 60 seconds animated
                                                explainer</p>
                                        </button>
                                    </a>

                                    <div class="remodal remodal--video-anim"
                                         data-remodal-id="watch-video-<?php echo get_row_layout() . '-' . get_row_index(); ?>">
                                        <button data-remodal-action="close" class="remodal__close">
                                            <span class="icon-close"></span>
                                        </button>

                                        <div class="remodal__container remodal__container--video">
                                            <div class="js-video" data-type="vimeo"
                                                 data-video-id="<?php echo substr( parse_url( $vimeo, PHP_URL_PATH ), 1 ); ?>"></div>
                                        </div>
                                    </div>
								<?php endif; ?>

                                <div class="how-it-works">
									<?php while ( have_rows( 'items' ) ) : the_row(); ?>
                                        <article class="how-it-works__item how-it-works__item--brands row">
                                            <div class="col-md-2">
                                                <img src="<?php echo get_template_directory_uri(); ?>/dist/images/how-it-works/pl-icon-<?php the_sub_field( 'icon' ); ?>.svg"
                                                     alt="<?php echo ucfirst( get_sub_field( 'icon' ) ); ?> Icon">
                                            </div>
                                            <div class="col-md-4">
                                                <p class="mb0"><?php the_sub_field( 'text' ); ?></p>
                                            </div>
                                        </article>
									<?php endwhile; ?>
                                </div>
                            </div>
                        </div>

                        <div class="container">
                            <h2 class="h1 text-center text-color-green mb100"><?php the_sub_field( 'pricing_title' ); ?></h2>

                            <div class="text-center">
                                <h4 class="text-regular mb30"><?php the_sub_field( 'pricing_subtitle' ); ?></h4>
                                <div class="h4 text-book mb100 mb50-sm-down"><?php the_sub_field( 'pricing_text' ); ?></div>

								<?php if ( $promo = get_sub_field( 'promo' ) ): ?>
                                    <div class="background-green-gradient p50 p25-sm-down mb100 mb50-sm-down">
										<?php echo $promo; ?>
                                    </div>
								<?php endif; ?>

                                <a href="<?= home_url() ?>/brand-submission" class="btn btn--large">Submit Your
                                    Brand</a>
                            </div>
                        </div>
                    </div>
				<?php endif; ?>
			<?php endwhile; ?>
        </section>
	<?php endif; ?>

	<?php if ( get_row_layout() == 'featured_brands_section' ) : ?>
        <section class="lp-brands mt100 mt50-sm-down mb100 mb50-sm-down pb100 pb50-sm-down border-bottom border-green-lightest">
            <div class="container">
                <h2 class="h1 text-center text-color-green mb100 mb50-sm-down"><?php the_sub_field( 'title' ); ?></h2>

                <div class="brands-carousel owl-carousel mb100 mb50-sm-down">
					<?php while ( have_rows( 'items' ) ) : the_row(); ?>
						<?php echo wp_get_attachment_image( get_sub_field( 'image' ), 'medium' ); ?>
					<?php endwhile; ?>
                </div>

                <div class="text-center">
                    <a href="<?= home_url() ?>/brand-submission" class="btn btn--large">Submit your brand</a>
                </div>
            </div>
        </section>
	<?php endif; ?>

	<?php if ( get_row_layout() == 'testimonials_section' ) : ?>
        <section class="lp-join mt100 mt50-sm-down mb100 mb50-sm-down">
            <div class="container">
                <h2 class="h1 text-center text-color-green mb100 mb50-sm-down"><?php the_sub_field( 'title' ); ?></h2>

                <div class="row flex--justify-center">
                    <div class="col-md-10">
                        <div class="testimonials-carousel owl-carousel">
							<?php while ( have_rows( 'items' ) ) : the_row(); ?>
                                <div class="testimonials-carousel__item text-center">
									<?php the_sub_field( 'testimonial' ); ?>
                                    <span><?php the_sub_field( 'brand' ); ?></span>
                                </div>
							<?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </div>
           
        </section>

	<?php endif; ?>

<?php endwhile; ?>
