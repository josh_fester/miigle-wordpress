<?php
/**
 * Template Name: Brand Page - Marketplace
 */

use Miigle\Models\Brand;
$badges = Brand\get_badges();
//$user = wp_get_current_user();
?>

<div id="template-brand_page" class="v2">
	<!-- Brand banner -->
	<section id="brand_page_hero" style="background-image:url('https://dev.miigle.com/wp-content/uploads/2018/07/brand-hero.jpg');">
      <div class="container h100">      
        <div class="row flex--justify-center flex--align-bottom h100">
          <div class="col-nano-8 text-center text-upper bgWhite85 pb1 pt1 sr-only">
            <!-- Social Impact Text -->
			<h3>Great job, you’re visiting a Social Good brand!</h3>
          </div>      	      	      	
        </div>
      </div>
    </section>
      
    <section id="brand_page_title">
      <div class="container">      
        <div class="row flex--justify-center">
          <div class="col-nano-10 col-md- text-center text-upper pt3">
            <!-- Brand name -->
			<h1>
			 	<?php the_title() ?> 
			 </h1>
          </div>      	      	      	
        </div>
        <div class="row flex--justify-center">
          <div class="col-nano-12 col-md-8 lead mb0 text-center pb3">
            <!-- Brand Description Text -->
			<p>
			  <?php echo Brand\brand_impact(get_the_ID()) ?>
			  </p>
          </div>      	      	      	
        </div>
      </div>
    </section>
      
    <section id="brand_page">
      <div class="container">      
        <div class="row">
          <div class="col-md-5 brand-media">
            <div class="brand--thumb pb1">
            <!-- Featured Image -->
			<img class="alignnone size-full wp-image-22558" src="https://dev.miigle.com/wp-content/uploads/2018/07/brand-media.png" alt="" srcset="https://dev.miigle.com/wp-content/uploads/2018/07/brand-media.png 703w, https://dev.miigle.com/wp-content/uploads/2018/07/brand-media-300x164.png 300w" sizes="(max-width: 703px) 100vw, 703px">
			<!-- If Video embed URL exsit replace with Video -->
            </div>
            <div class="brand--tags">
			<!-- Badges -->
			
			<a href="https://dev.miigle.com/badge/popular/" class="btn btn-tags text-upper" title="Popular">Popular</a>
			<a href="https://dev.miigle.com/badge/vegan/" class="btn btn-tags text-upper" title="Vegan">Vegan</a>
			<?php 
			$bTags = get_field('mkp_brand_info_brand_tags');
			if( $bTags ): ?>
				<?php foreach( $bTags as $bTag ): 
					$term_link = get_term_link( $bTag );
					if ( is_wp_error( $term_link ) ) {
						continue;
					}
				?>
				<a href="<?php echo $term_link; ?>" class="btn btn-tags text-upper" title="<?php echo $bTag->name; ?>"><?php echo $bTag->name; ?></a>
				<?php endforeach; ?>
			<?php endif; ?>
            </div>
          </div>
          <div class="col-md-7 brand-data">
            <div class="prod--description">
              <div class="prod--entry">
                <?php the_content(); ?>
				</div>
            </div>
        </div>      	      	      	
        </div>
      </div>
    </section>
    
	<!-- If Brand Quote Text Exisit SHOW this section -->
    <section id="brand_page_quote" class="mt5">
      <div class="container">      
        <div class="row">
          <div class="col-nano-12 text-center quoteText border pt3 pb3">
            <!-- Brand Quote Text -->
			  <p>“The bigger we get, the more milestones we hit, we realize there is such a big opportunity in front of us, we are just motivated to continue to grow and have more impact over time.”</p>
          </div>
        </div>
      </div>
    </section>
	
	    <section id="section--search-bar" class="section--pad">  
      <div class="container">            
        <div class="search-wrapper">
          <form id="product-search">
            <div class="row">
              <div class="col-nano-9">
              <div class="input-group">
                <span class="input-group-addon"><img src="/wp-content/uploads/2018/07/icon-search.svg"></span>
                <input type="search" class="form-control" name="s" placeholder="Search ethical products and brands…">
                <!--<input type="search" class="form-control" name="s" placeholder="Search ethical products and brands…" onfocus="if (this.value == 'Search') { this.value = ''; }" onblur="if (this.value == '') this.value='Search';">-->
              </div>
            </div>
              <div class="col-nano-3">
                <button type="submit" class="btn btn-default btn-submit">SEARCH</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
      
    <section id="section--similar-products" class="section--pad">
      <div class="container">
        <!-- FILTER ROW -->
        <div class="row" id="filter-product">
          <div class="col-nano-12 text-center text-upper pt1 pb2">
            <span class="cat-filter active"><a href="#all">all</a></span>
            <span class="cat-filter "><a href="#accessories">accessories</a></span>
            <span class="cat-filter "><a href="#gift">gift</a></span>
          </div>
        </div>

        <div class="row masonry-container">
        <div class="col-nano-12 col-micro-6 col-xs-6 col-sm-6 col-md-4 col-lg-3 masonry-sizer"></div><!-- left empty for column sizing -->

          <div class="col-lg-3 col-md-4 col-nano-6 masonry-item">
            <div class="thumbnail">
              <img src="/wp-content/uploads/2018/07/thumb1.png" class="">
              <div class="caption">
                <p class="brand">Johnson &amp; Johnson</p>
                <h3>Fitmyfavo Backpack</h3>
                <p class="price">$35.00</p>
              </div>
              <a href="product_page.html"><div class="mask"></div></a>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-nano-6 masonry-item">
            <div class="thumbnail">
              <img src="/wp-content/uploads/2018/07/thumb2.png" class="">
              <div class="caption">
                <p class="brand">Johnson &amp; Johnson</p>
                <h3>Fitmyfavo Backpack</h3>
                <p class="price">$35.00</p>
              </div>
              <a href="product_page.html"><div class="mask"></div></a>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-nano-6 masonry-item">
            <div class="thumbnail">
              <img src="/wp-content/uploads/2018/07/thumb3.png" class="">
              <div class="caption">
                <p class="brand">Johnson &amp; Johnson</p>
                <h3>Fitmyfavo Backpack</h3>
                <p class="price">$35.00</p>
              </div>
              <a href="product_page.html"><div class="mask"></div></a>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-nano-6 masonry-item">
            <div class="thumbnail">
              <img src="/wp-content/uploads/2018/07/thumb4.png" class="">
              <div class="caption">
                <p class="brand">Johnson &amp; Johnson</p>
                <h3>Fitmyfavo Backpack</h3>
                <p class="price">$35.00</p>
              </div>
              <a href="product_page.html"><div class="mask"></div></a>
            </div>
          </div>

        </div>

        <div class="row view-more" id="viewProducts">
          <div class="col-nano-12 text-center">
            <a class="btn-view-more text-upper" href="#view-more-products">View More</a>
          </div>
        </div>

      </div>
    </section>

</div>