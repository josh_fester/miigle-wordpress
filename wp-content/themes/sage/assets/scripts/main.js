// Mixpanel init
(function (e, b) {
    if (!b.__SV) {
        var a, f, i, g;
        window.mixpanel = b;
        b._i = [];
        b.init = function (a, e, d) {
            function f(b, h) {
                var a = h.split(".");
                2 == a.length && (b = b[a[0]], h = a[1]);
                b[h] = function () {
                    b.push([h].concat(Array.prototype.slice.call(arguments, 0)))
                }
            }

            var c = b;
            "undefined" !== typeof d ? c = b[d] = [] : d = "mixpanel";
            c.people = c.people || [];
            c.toString = function (b) {
                var a = "mixpanel";
                "mixpanel" !== d && (a += "." + d);
                b || (a += " (stub)");
                return a
            };
            c.people.toString = function () {
                return c.toString(1) + ".people (stub)"
            };
            i = "disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
            for (g = 0; g < i.length; g++)f(c, i[g]);
            b._i.push([a, e, d])
        };
        b.__SV = 1.2;
        a = e.createElement("script");
        a.type = "text/javascript";
        a.async = !0;
        a.src = "undefined" !== typeof MIXPANEL_CUSTOM_LIB_URL ? MIXPANEL_CUSTOM_LIB_URL : "file:" === e.location.protocol && "//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//) ? "https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js" : "//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";
        f = e.getElementsByTagName("script")[0];
        f.parentNode.insertBefore(a, f)
    }
})(document, window.mixpanel || []);

//jquery.cookieBar.min.js
(function(e){e.cookie=function(o,i,n){if(arguments.length>1&&(!/Object/.test(Object.prototype.toString.call(i))||null===i||void 0===i)){if(n=e.extend({},n),null!==i&&void 0!==i||(n.expires=-1),"number"==typeof n.expires){var t=n.expires,r=n.expires=new Date;r.setDate(r.getDate()+t)}return i=String(i),document.cookie=[encodeURIComponent(o),"=",n.raw?i:encodeURIComponent(i),n.expires?"; expires="+n.expires.toUTCString():"",n.path?"; path="+n.path:"",n.domain?"; domain="+n.domain:"",n.secure?"; secure":""].join("")}n=i||{};for(var c,s=n.raw?function(e){return e}:decodeURIComponent,a=document.cookie.split("; "),u=0;c=a[u]&&a[u].split("=");u++)if(s(c[0])===o)return s(c[1]||"");return null},e.fn.cookieBar=function(o){var i=e.extend({closeButton:"none",hideOnClose:!0,secure:!1,path:"/",domain:""},o);return this.each(function(){var o=e(this);o.hide(),"none"==i.closeButton&&(o.append('<a class="cookiebar-close">Continue</a>'),e.extend(i,{closeButton:".cookiebar-close"})),"hide"!=e.cookie("cookiebar")&&o.show(),o.find(i.closeButton).click(function(){return i.hideOnClose&&o.hide(),e.cookie("cookiebar","hide",{path:i.path,secure:i.secure,domain:i.domain,expires:30}),o.trigger("cookieBar-close"),!1})})},e.cookieBar=function(o){e("body").prepend('<div class="ui-widget"><div style="display: none;" class="cookie-message ui-widget-header blue"><p>By using this website you allow us to place cookies on your computer. They are harmless and never personally identify you.</p></div></div>'),e(".cookie-message").cookieBar(o)}})(jQuery);

/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function ($) {

    // Use this variable to set up the common and page specific functions. If you
    // rename this variable, you will also need to rename the namespace below.
    var Sage = {
        // All pages
        'common': {
            init: function () {

                mixpanel.init("0115ab26ea2dfdb786fb487d17abb428");
                mixpanel.track('page viewed', {
                    'page name': document.title,
                    'url': window.location.pathname
                });

                $(window).load(function () {
                    //$('.grid').masonry({
                    //    itemSelector: '.grid-item', // use a separate class for itemSelector, other than .col-
                    //    columnWidth: '.grid-sizer',
                    //    percentPosition: true
                    //});
                    $(".dotdotdot").dotdotdot({
                        wrap: 'letter',
                        height: 44
                    });
                });

                $('.btn-upvote').on('click', function (e) {
                    e.preventDefault();
                    var $star = $(this).find('i');
                    var $form = $(this).parents('form');
                    var upvotes = $form.find('span#upvotes').html() * 1;
                    var $this = $(this);

                    // just upvoted
                    if ($form.attr('action') === 'mgl/v1/product/upvote') {
                        $this.addClass('upvoted');
                        $form.find('span#upvotes').html(upvotes + 1);
                    }
                    // just downvoted
                    else {
                        $this.removeClass('upvoted');
                        $form.find('span#upvotes').html(upvotes - 1);
                    }

                    apiAjax($form, $form.serialize())
                        .then(function (success) {
                            // just upvoted
                            if ($form.attr('action') === 'mgl/v1/product/upvote') {
                                $form.attr('action', 'mgl/v1/product/downvote');
                            }
                            // just downvoted
                            else {
                                $form.attr('action', 'mgl/v1/product/upvote');
                            }
                        });

                    return false;
                });
				
				//Toggle show more Brands - Marketplace
				$(document).ready(function(){
				   var moretext = "View More";
				   var lesstext = "View Less";
				   var hiddencard = ".hidden-sm-card";

				   $("#viewBrands .btn-view-more").click(function(){
						if($(this).hasClass("less")) {
							$(this).removeClass("less");
							$(this).html(moretext);
							$(hiddencard).fadeOut("slow");
						} else {
							$(this).addClass("less");
							$(this).html(lesstext);
							$(hiddencard).fadeIn("slow");
						}
						$(this).parent().prev().toggle();
						$(this).prev().toggle();
						return false;
					});

				});

				//Toggle read more Text - Marketplace
				$(document).ready(function(){
				   $('a.readMore').click(function(){
					 $(this).prev().slideToggle(function() {
					   $(this).next('a.readMore').text(function (index, text) {
						 return (text == 'Read More' ? 'Read Less' : 'Read More');
					   });
					 });

					 return false;
				   });

				});
				
				//Product Gallery - Marketplace
				$(document).ready(function(){
					$('.previews a').click(function(){
					  var largeImage = $(this).attr('data-full');
					  $('.selected').removeClass();
					  $(this).addClass('selected');
					  $('.full img').hide();
					  $('.full img').attr('src', largeImage);
					  $('.full img').fadeIn();
					});
				  });
                
                //Homepage Notification Bar 
                $(document).ready(function(){
                  if ($("#nBar").length > 0) {             
                    $('#nBar').cookieBar({ closeButton : '.notice__close', expires: 7 });
                    
                    if ($('#nBar').is(":visible")) {
                      $('body').addClass('notice-present');
				      $('.primary-nav').css('top', $('.notice').outerHeight()+'px');
                      $('#section--hero').css('margin-top', $('.notice').outerHeight()+'px');
                    }
                    $('.notice__close').click(function() {
                      $('body').removeClass('notice-present');
                      $('#nBar').remove();
                      $('.primary-nav').css('top', '');
                      $('#section--hero').css('margin-top', '');
                    });
                  }
                });                   

            },
            finalize: function () {
                // JavaScript to be fired on all pages, after page specific JS is fired
            }
        },

        // Home page
        'home': {
            init: function () {
                $('#accessModal form').on('submit', function (e) {
                    console.log('ayyy');
                    e.preventDefault();
                    $input = $('input#accessCode');

                    if ($input.val() === 'M+2016') {
                        $input.parents('.form-group')
                            .removeClass('has-error')
                            .addClass('has-success')
                            .find('.help-block')
                            .addClass('hidden');
                        window.location = '/products';
                    }
                    else {
                        console.log('nope');
                        $input.parents('.form-group')
                            .addClass('has-error')
                            .find('.help-block')
                            .removeClass('hidden');
                    }

                    return false;
                });
            },
            finalize: function () {
                // JavaScript to be fired on the home page, after the init JS
            }
        },

        // new Home page
        'homepage': {
            init: function () {
                $('#accessModal form').on('submit', function (e) {
                    console.log('ayyy');
                    e.preventDefault();
                    $input = $('input#accessCode');

                    if ($input.val() === 'M+2016') {
                        $input.parents('.form-group')
                            .removeClass('has-error')
                            .addClass('has-success')
                            .find('.help-block')
                            .addClass('hidden');
                        window.location = '/marketplace';
                    }
                    else {
                        console.log('nope');
                        $input.parents('.form-group')
                            .addClass('has-error')
                            .find('.help-block')
                            .removeClass('hidden');
                    }

                    return false;
                });
            },
            finalize: function () {
                // JavaScript to be fired on the home page, after the init JS
            }
        },

        'sign_up': {
            init: function () {
                var $form = $('form#signup-form'),
                    data;

                $form.on('submit', function (e) {
                    e.preventDefault();
                    $form.validate();

                    if ($form.valid()) {
                        data = {
                            username: $form.find('input[name=email]').val(),
                            email: $form.find('input[name=email]').val(),
                            first_name: $form.find('input[name=firstName]').val(),
                            last_name: $form.find('input[name=lastName]').val(),
                            password: $form.find('input[name=password]').val()
                        };

                        apiAjax($form, data);
                    }

                    return false;
                });
            }
        },

        'product_post': {
            init: function () {
                var $requestForm = $('form#request-invite'),
                    $productForm = $('form#product'),
                    $modal = $('#post-success');

                $modal.on('hidden.bs.modal', function (e) {
                    window.location = wpHomeUrl + '/profile-product';
                });

                $requestForm.on('submit', function (e) {
                    e.preventDefault();
                    $requestForm.validate();

                    if ($requestForm.valid()) {
                        apiAjax($requestForm, $requestForm.serialize())
                            .then(function (success) {
                                window.location = wpHomeUrl + '/profile-product';
                            });
                    }

                    return false;
                });

                $productForm.on('submit', function (e) {
                    e.preventDefault();
                    $productForm.validate();

                    if ($productForm.valid()) {
                        apiAjax($productForm, $productForm.serialize())
                            .then(function (success) {
                                $modal.modal('show');
                            });
                    }

                    return false;
                });

                $productForm.find('#category .category-list input').on('change', function (e) {
                    var $target = $(e.target),
                        parentId = $target.attr('id'),
                        hasSubCategory = false;

                    $productForm.find('#category input:checked').each(function () {
                        var id = $(this).attr('id');
                        $subCategory = $productForm.find('#sub-category input.' + id);
                        console.log($subCategory);
                        if ($subCategory.length !== 0) {
                            hasSubCategory = true;
                            return false;
                        }
                        else {
                            hasSubCategory = false;
                        }
                    });

                    if (hasSubCategory) {
                        $productForm.find('#category a').attr('href', '#sub-category');
                        $productForm.find('#details a.back').attr('href', '#sub-category');
                    }
                    else {
                        $productForm.find('#category a').attr('href', '#details');
                        $productForm.find('#details a.back').attr('href', '#category');
                    }

                    $productForm.find('#sub-category .sub-category').addClass('hidden');

                    if ($target.is(':checked')) {
                        $productForm.find('#sub-category input.' + parentId).parents('.sub-category').removeClass('hidden');
                    }
                });

                $productForm.find('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    var targetId = $(e.target).attr('href'),
                        percentDone = '25%';

                    if (targetId === '#sub-category') {
                        percentDone = '50%';
                    }
                    else if (targetId === '#details') {
                        percentDone = '75%';
                    }

                    $('.progress-bar').css('width', percentDone);
                    $('span#percent-complete').html(percentDone);

                });

            }
        },

        'profile_settings': {
            init: function () {
                var $form = $('form#profile-settings');

                $form.on('submit', function (e) {
                    e.preventDefault();
                    $form.validate();

                    if ($form.valid()) {
                        apiAjax($form, $form.serialize())
                            .then(function (success) {
                                window.location = wpHomeUrl + '/profile-product';
                            });
                    }

                    return false;
                });
            }
        },

        'single_mgl_product': {
            init: function () {
                $('.prod-gallery .previews a').on('click', function (e) {
                    e.preventDefault();
                    var $this = $(this);
                    var img = $this.data('img');

                    $('.prod-gallery .previews a').removeClass('active');
                    $this.addClass('active');

                    $('.prod-gallery .full img').attr('src', img);
                });
            }
        },

        'directory_submission': {
            init: function () {
                var authorization = $('#braintree-client-token').data('token');
                var submit = document.querySelector('button[type="submit"]');
                var form = document.querySelector('#checkout-form');
                var $checkoutForm = $('form#checkout-form');
                var paypalRadioButton = $('input#paypal');

                var selectedPlan = {};
                var plans;

                $.ajax({
                    url: '/wp-json/mgl/v1/subscription/'
                }).done(function (data) {
                    plans = data;
                    var lastPlan = plans[Object.keys(plans)[Object.keys(plans).length - 1]];

                    selectedPlan.price = lastPlan.price;

                    $('.checkout-total__price').text(lastPlan.price);
                    $('.checkout-plan__name').text(lastPlan.name);

                    $.each(data, function (index, value) {
                        $('.checkout-pricing-items').append(
                            '<div class="col-md-3 checkout-pricing-item">' +
                            '<input type="radio" name="plan" id="' + index + '" value="' + index + '" ' + (lastPlan.name === value.name ? 'checked' : '') + '>' +
                            '<label for="' + index + '"><strong>' + value.name + '</strong> - $ ' + value.price + '</label>' +
                            '</div>'
                        )
                    });
                });

                $('.checkout-cards-fields__code .btn').on('click', function () {
                    var button_id;
                    if ($('#promo-code').prop('disabled')) {
                        button_id = $('#paypal-promo-code');
                    } else {
                        button_id = $('#promo-code');
                    }
                    var code = button_id.val();

                    $.ajax({
                        url: '/wp-json/mgl/v1/subscription/discount/' + code
                    }).done(function (data) {
                        if (data === false) {
                            button_id.addClass('has-error').removeClass('has-success');
                        } else {
                            button_id.addClass('has-success').removeClass('has-error');

                            $('.checkout-total__price').text(function () {
                                $('.checkout-total__price').text(Math.max(0, Number(selectedPlan.price - data.amount).toFixed(2)))
                            });
                        }
                    }).fail(function (data) {
                        button_id.addClass('has-error').removeClass('has-success');
                    });
                });

                $(document).on('change', '.checkout-pricing-item input', function (e) {
                    console.log('pricing change ::::::::::::::::::');
                    console.log('selectedPlan:');
                    console.log(selectedPlan);
                    console.log('plans:');
                    console.log(plans);
                    console.log('target:');
                    console.log(e.target);
                    console.log('checkoutForm:');
                    console.log($checkoutForm);
                    selectedPlan = plans[e.target.value];

                    if (selectedPlan) {
                        $('.checkout-total__price').text(selectedPlan.price);
                        $('.checkout-plan__name').text(selectedPlan.name);
                    }
                });

                $('.checkout-cards input').bind('change', function (e) {
                    if (e.target.value !== 'paypal') {
                        $('.checkout-cards-fields').show();
                        $('.checkout-paypal-fields').hide();
                        $('#promo-code').attr('disabled', false);
                        $('#paypal-promo-code').attr('disabled', true);
                    } else {
                        $('.checkout-cards-fields').hide();
                        $('.checkout-paypal-fields').show();
                        $('#promo-code').attr('disabled', true);
                        $('#paypal-promo-code').attr('disabled', false);
                    }
                });

                braintree.client.create({
                    authorization: authorization
                }, function (clientErr, clientInstance) {
                    if (clientErr) {
                        // Handle error in client creation
                        return;
                    }

                    braintree.hostedFields.create({
                        client: clientInstance,
                        styles: {
                            'input': {
                                'font-size': '18px',
                                'color': '#999',
                                'font-family': '"brandon-grotesque",sans-serif',
                                'font-weight': '100'
                            },
                            'input.invalid': {
                                'color': 'red'
                            },
                            'input.valid': {
                                'color': 'green'
                            }
                        },
                        fields: {
                            number: {
                                selector: '#card-number',
                                placeholder: 'Card Number'
                            },
                            cvv: {
                                selector: '#cvv',
                                placeholder: 'CVV Num'
                            },
                            expirationDate: {
                                selector: '#expiration-date',
                                placeholder: 'Expiration (MM/YYYY)'
                            },
                            postalCode: {
                                selector: '#postal-code',
                                placeholder: '11111'
                            }
                        }
                    }, function (hostedFieldsErr, hostedFieldsInstance) {
                        if (hostedFieldsErr) {
                            // Handle error in Hosted Fields creation
                            return;
                        }

                        submit.removeAttribute('disabled');
                        form.addEventListener('submit', function (event) {
                            event.preventDefault();
                            if (!paypalRadioButton.is(':checked')) {

                                $('.checkout-total__loader').removeClass('hidden');

                                hostedFieldsInstance.tokenize(function (tokenizeErr, payload) {
                                    if (tokenizeErr) {
                                        // Handle error in Hosted Fields tokenization
                                        console.log('Error tokenizing:', tokenizeErr);
                                        $('.checkout-total__loader').addClass('hidden');
                                        return;
                                    }
                                    // Put `payload.nonce` into the `payment-method-nonce` input, and then
                                    // submit the form. Alternatively, you could send the nonce to your server
                                    // with AJAX.
                                    document.querySelector('input[name="payment-method-nonce"]').value = payload.nonce;

                                    apiAjax($checkoutForm, $checkoutForm.serialize())
                                        .then(function (success) {
                                            console.log('success');
                                            console.log(success);
                                            window.location = wpHomeUrl + '/directory-brand-submit-confirmation';
                                        });

                                });
                            }
                        }, false);

                    });

                    braintree.paypal.create({
                        client: clientInstance
                    }, function (paypalErr, paypalInstance) {

                        // Stop if there was a problem creating PayPal.
                        // This could happen if there was a network error or if it's incorrectly
                        // configured.
                        if (paypalErr) {
                            console.error('Error creating PayPal:', paypalErr);
                            return;
                        }

                        // When the button is clicked, attempt to tokenize.
                        form.addEventListener('submit', function (event) {
                            event.preventDefault();
                            if (paypalRadioButton.is(':checked')) {

                                $('.checkout-total__loader').removeClass('hidden');

                                // Because tokenization opens a popup, this has to be called as a result of
                                // customer action, like clicking a button—you cannot call this at any time.
                                paypalInstance.tokenize({
                                    flow: 'vault'
                                }, function (tokenizeErr, payload) {

                                    // Stop if there was an error.
                                    if (tokenizeErr) {
                                        if (tokenizeErr.type !== 'CUSTOMER') {
                                            console.error('Error tokenizing:', tokenizeErr);
                                        }
                                        $('.checkout-total__loader').addClass('hidden');
                                        return;
                                    }

                                    document.querySelector('input[name="payment-method-nonce"]').value = payload.nonce;

                                    apiAjax($checkoutForm, $checkoutForm.serialize())
                                        .then(function (success) {
                                            console.log('success');
                                            console.log(success);
                                            window.location = wpHomeUrl + '/directory-brand-submit-confirmation';
                                        });

                                });

                            }
                        }, false);

                    });
                });
            }
        }

    };

    var apiAjax = function ($form, data) {
        return $.ajax({
            url: wpApiSettings.root + $form.attr('action'),
            method: $form.attr('method'),
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
                $form.find('[type=submit]')
                    .attr('disabled', 'disabled')
                    .find('.fa-spin')
                    .removeClass('hidden');
            },
            complete: function () {
                $form.find('[type=submit]')
                    .removeAttr('disabled')
                    .find('.fa-spin')
                    .addClass('hidden');
            }
        });
    };

    // The routing fires all common scripts, followed by the page specific scripts.
    // Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function (func, funcname, args) {
            var fire;
            var namespace = Sage;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

            if (fire) {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function () {
            // Fire common init JS
            UTIL.fire('common');

            // Fire page-specific init JS, and then finalize JS
            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
                UTIL.fire(classnm);
                UTIL.fire(classnm, 'finalize');
            });

            // Fire common finalize JS
            UTIL.fire('common', 'finalize');
        }
    };

    // Load Events
    $(document).ready(UTIL.loadEvents);


    /*!
     * jQuery Mousewheel 3.1.13
     *
     * Copyright 2015 jQuery Foundation and other contributors
     * Released under the MIT license.
     * http://jquery.org/license
     */
    !function (a) {
        "function" == typeof define && define.amd ? define(["jquery"], a) : "object" == typeof exports ? module.exports = a : a(jQuery)
    }(function (a) {
        function b(b) {
            var g = b || window.event, h = i.call(arguments, 1), j = 0, l = 0, m = 0, n = 0, o = 0, p = 0;
            if (b = a.event.fix(g), b.type = "mousewheel", "detail" in g && (m = -1 * g.detail), "wheelDelta" in g && (m = g.wheelDelta), "wheelDeltaY" in g && (m = g.wheelDeltaY), "wheelDeltaX" in g && (l = -1 * g.wheelDeltaX), "axis" in g && g.axis === g.HORIZONTAL_AXIS && (l = -1 * m, m = 0), j = 0 === m ? l : m, "deltaY" in g && (m = -1 * g.deltaY, j = m), "deltaX" in g && (l = g.deltaX, 0 === m && (j = -1 * l)), 0 !== m || 0 !== l) {
                if (1 === g.deltaMode) {
                    var q = a.data(this, "mousewheel-line-height");
                    j *= q, m *= q, l *= q
                } else if (2 === g.deltaMode) {
                    var r = a.data(this, "mousewheel-page-height");
                    j *= r, m *= r, l *= r
                }
                if (n = Math.max(Math.abs(m), Math.abs(l)), (!f || f > n) && (f = n, d(g, n) && (f /= 40)), d(g, n) && (j /= 40, l /= 40, m /= 40), j = Math[j >= 1 ? "floor" : "ceil"](j / f), l = Math[l >= 1 ? "floor" : "ceil"](l / f), m = Math[m >= 1 ? "floor" : "ceil"](m / f), k.settings.normalizeOffset && this.getBoundingClientRect) {
                    var s = this.getBoundingClientRect();
                    o = b.clientX - s.left, p = b.clientY - s.top
                }
                return b.deltaX = l, b.deltaY = m, b.deltaFactor = f, b.offsetX = o, b.offsetY = p, b.deltaMode = 0, h.unshift(b, j, l, m), e && clearTimeout(e), e = setTimeout(c, 200), (a.event.dispatch || a.event.handle).apply(this, h)
            }
        }

        function c() {
            f = null
        }

        function d(a, b) {
            return k.settings.adjustOldDeltas && "mousewheel" === a.type && b % 120 === 0
        }

        var e, f, g = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"], h = "onwheel" in document || document.documentMode >= 9 ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"], i = Array.prototype.slice;
        if (a.event.fixHooks)for (var j = g.length; j;)a.event.fixHooks[g[--j]] = a.event.mouseHooks;
        var k = a.event.special.mousewheel = {
            version: "3.1.12", setup: function () {
                if (this.addEventListener)for (var c = h.length; c;)this.addEventListener(h[--c], b, !1); else this.onmousewheel = b;
                a.data(this, "mousewheel-line-height", k.getLineHeight(this)), a.data(this, "mousewheel-page-height", k.getPageHeight(this))
            }, teardown: function () {
                if (this.removeEventListener)for (var c = h.length; c;)this.removeEventListener(h[--c], b, !1); else this.onmousewheel = null;
                a.removeData(this, "mousewheel-line-height"), a.removeData(this, "mousewheel-page-height")
            }, getLineHeight: function (b) {
                var c = a(b), d = c["offsetParent" in a.fn ? "offsetParent" : "parent"]();
                return d.length || (d = a("body")), parseInt(d.css("fontSize"), 10) || parseInt(c.css("fontSize"), 10) || 16
            }, getPageHeight: function (b) {
                return a(b).height()
            }, settings: {adjustOldDeltas: !0, normalizeOffset: !0}
        };
        a.fn.extend({
            mousewheel: function (a) {
                return a ? this.bind("mousewheel", a) : this.trigger("mousewheel")
            }, unmousewheel: function (a) {
                return this.unbind("mousewheel", a)
            }
        })
    });

    /*!
     * jScrollPane - v2.0.23 - 2016-01-28
     * http://jscrollpane.kelvinluck.com/
     *
     * Copyright (c) 2014 Kelvin Luck
     * Dual licensed under the MIT or GPL licenses.
     */
    !function (a) {
        "function" == typeof define && define.amd ? define(["jquery"], a) : "object" == typeof exports ? module.exports = a(require("jquery")) : a(jQuery)
    }(function (a) {
        a.fn.jScrollPane = function (b) {
            function c(b, c) {
                function d(c) {
                    var f, h, j, k, l, o, p = !1, q = !1;
                    if (N = c, void 0 === O) l = b.scrollTop(), o = b.scrollLeft(), b.css({
                        overflow: "hidden",
                        padding: 0
                    }), P = b.innerWidth() + rb, Q = b.innerHeight(), b.width(P), O = a('<div class="jspPane" />').css("padding", qb).append(b.children()), R = a('<div class="jspContainer" />').css({
                        width: P + "px",
                        height: Q + "px"
                    }).append(O).appendTo(b); else {
                        if (b.css("width", ""), p = N.stickToBottom && A(), q = N.stickToRight && B(), k = b.innerWidth() + rb != P || b.outerHeight() != Q, k && (P = b.innerWidth() + rb, Q = b.innerHeight(), R.css({
                                width: P + "px",
                                height: Q + "px"
                            })), !k && sb == S && O.outerHeight() == T)return void b.width(P);
                        sb = S, O.css("width", ""), b.width(P), R.find(">.jspVerticalBar,>.jspHorizontalBar").remove().end()
                    }
                    O.css("overflow", "auto"), S = c.contentWidth ? c.contentWidth : O[0].scrollWidth, T = O[0].scrollHeight, O.css("overflow", ""), U = S / P, V = T / Q, W = V > 1, X = U > 1, X || W ? (b.addClass("jspScrollable"), f = N.maintainPosition && ($ || bb), f && (h = y(), j = z()), e(), g(), i(), f && (w(q ? S - P : h, !1), v(p ? T - Q : j, !1)), F(), C(), L(), N.enableKeyboardNavigation && H(), N.clickOnTrack && m(), J(), N.hijackInternalLinks && K()) : (b.removeClass("jspScrollable"), O.css({
                            top: 0,
                            left: 0,
                            width: R.width() - rb
                        }), D(), G(), I(), n()), N.autoReinitialise && !pb ? pb = setInterval(function () {
                            d(N)
                        }, N.autoReinitialiseDelay) : !N.autoReinitialise && pb && clearInterval(pb), l && b.scrollTop(0) && v(l, !1), o && b.scrollLeft(0) && w(o, !1), b.trigger("jsp-initialised", [X || W])
                }

                function e() {
                    W && (R.append(a('<div class="jspVerticalBar" />').append(a('<div class="jspCap jspCapTop" />'), a('<div class="jspTrack" />').append(a('<div class="jspDrag" />').append(a('<div class="jspDragTop" />'), a('<div class="jspDragBottom" />'))), a('<div class="jspCap jspCapBottom" />'))), cb = R.find(">.jspVerticalBar"), db = cb.find(">.jspTrack"), Y = db.find(">.jspDrag"), N.showArrows && (hb = a('<a class="jspArrow jspArrowUp" />').bind("mousedown.jsp", k(0, -1)).bind("click.jsp", E), ib = a('<a class="jspArrow jspArrowDown" />').bind("mousedown.jsp", k(0, 1)).bind("click.jsp", E), N.arrowScrollOnHover && (hb.bind("mouseover.jsp", k(0, -1, hb)), ib.bind("mouseover.jsp", k(0, 1, ib))), j(db, N.verticalArrowPositions, hb, ib)), fb = Q, R.find(">.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow").each(function () {
                        fb -= a(this).outerHeight()
                    }), Y.hover(function () {
                        Y.addClass("jspHover")
                    }, function () {
                        Y.removeClass("jspHover")
                    }).bind("mousedown.jsp", function (b) {
                        a("html").bind("dragstart.jsp selectstart.jsp", E), Y.addClass("jspActive");
                        var c = b.pageY - Y.position().top;
                        return a("html").bind("mousemove.jsp", function (a) {
                            p(a.pageY - c, !1)
                        }).bind("mouseup.jsp mouseleave.jsp", o), !1
                    }), f())
                }

                function f() {
                    db.height(fb + "px"), $ = 0, eb = N.verticalGutter + db.outerWidth(), O.width(P - eb - rb);
                    try {
                        0 === cb.position().left && O.css("margin-left", eb + "px")
                    } catch (a) {
                    }
                }

                function g() {
                    X && (R.append(a('<div class="jspHorizontalBar" />').append(a('<div class="jspCap jspCapLeft" />'), a('<div class="jspTrack" />').append(a('<div class="jspDrag" />').append(a('<div class="jspDragLeft" />'), a('<div class="jspDragRight" />'))), a('<div class="jspCap jspCapRight" />'))), jb = R.find(">.jspHorizontalBar"), kb = jb.find(">.jspTrack"), _ = kb.find(">.jspDrag"), N.showArrows && (nb = a('<a class="jspArrow jspArrowLeft" />').bind("mousedown.jsp", k(-1, 0)).bind("click.jsp", E), ob = a('<a class="jspArrow jspArrowRight" />').bind("mousedown.jsp", k(1, 0)).bind("click.jsp", E), N.arrowScrollOnHover && (nb.bind("mouseover.jsp", k(-1, 0, nb)), ob.bind("mouseover.jsp", k(1, 0, ob))), j(kb, N.horizontalArrowPositions, nb, ob)), _.hover(function () {
                        _.addClass("jspHover")
                    }, function () {
                        _.removeClass("jspHover")
                    }).bind("mousedown.jsp", function (b) {
                        a("html").bind("dragstart.jsp selectstart.jsp", E), _.addClass("jspActive");
                        var c = b.pageX - _.position().left;
                        return a("html").bind("mousemove.jsp", function (a) {
                            r(a.pageX - c, !1)
                        }).bind("mouseup.jsp mouseleave.jsp", o), !1
                    }), lb = R.innerWidth(), h())
                }

                function h() {
                    R.find(">.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow").each(function () {
                        lb -= a(this).outerWidth()
                    }), kb.width(lb + "px"), bb = 0
                }

                function i() {
                    if (X && W) {
                        var b = kb.outerHeight(), c = db.outerWidth();
                        fb -= b, a(jb).find(">.jspCap:visible,>.jspArrow").each(function () {
                            lb += a(this).outerWidth()
                        }), lb -= c, Q -= c, P -= b, kb.parent().append(a('<div class="jspCorner" />').css("width", b + "px")), f(), h()
                    }
                    X && O.width(R.outerWidth() - rb + "px"), T = O.outerHeight(), V = T / Q, X && (mb = Math.ceil(1 / U * lb), mb > N.horizontalDragMaxWidth ? mb = N.horizontalDragMaxWidth : mb < N.horizontalDragMinWidth && (mb = N.horizontalDragMinWidth), _.width(mb + "px"), ab = lb - mb, s(bb)), W && (gb = Math.ceil(1 / V * fb), gb > N.verticalDragMaxHeight ? gb = N.verticalDragMaxHeight : gb < N.verticalDragMinHeight && (gb = N.verticalDragMinHeight), Y.height(gb + "px"), Z = fb - gb, q($))
                }

                function j(a, b, c, d) {
                    var e, f = "before", g = "after";
                    "os" == b && (b = /Mac/.test(navigator.platform) ? "after" : "split"), b == f ? g = b : b == g && (f = b, e = c, c = d, d = e), a[f](c)[g](d)
                }

                function k(a, b, c) {
                    return function () {
                        return l(a, b, this, c), this.blur(), !1
                    }
                }

                function l(b, c, d, e) {
                    d = a(d).addClass("jspActive");
                    var f, g, h = !0, i = function () {
                        0 !== b && tb.scrollByX(b * N.arrowButtonSpeed), 0 !== c && tb.scrollByY(c * N.arrowButtonSpeed), g = setTimeout(i, h ? N.initialDelay : N.arrowRepeatFreq), h = !1
                    };
                    i(), f = e ? "mouseout.jsp" : "mouseup.jsp", e = e || a("html"), e.bind(f, function () {
                        d.removeClass("jspActive"), g && clearTimeout(g), g = null, e.unbind(f)
                    })
                }

                function m() {
                    n(), W && db.bind("mousedown.jsp", function (b) {
                        if (void 0 === b.originalTarget || b.originalTarget == b.currentTarget) {
                            var c, d = a(this), e = d.offset(), f = b.pageY - e.top - $, g = !0, h = function () {
                                var a = d.offset(), e = b.pageY - a.top - gb / 2, j = Q * N.scrollPagePercent, k = Z * j / (T - Q);
                                if (0 > f) $ - k > e ? tb.scrollByY(-j) : p(e); else {
                                    if (!(f > 0))return void i();
                                    e > $ + k ? tb.scrollByY(j) : p(e)
                                }
                                c = setTimeout(h, g ? N.initialDelay : N.trackClickRepeatFreq), g = !1
                            }, i = function () {
                                c && clearTimeout(c), c = null, a(document).unbind("mouseup.jsp", i)
                            };
                            return h(), a(document).bind("mouseup.jsp", i), !1
                        }
                    }), X && kb.bind("mousedown.jsp", function (b) {
                        if (void 0 === b.originalTarget || b.originalTarget == b.currentTarget) {
                            var c, d = a(this), e = d.offset(), f = b.pageX - e.left - bb, g = !0, h = function () {
                                var a = d.offset(), e = b.pageX - a.left - mb / 2, j = P * N.scrollPagePercent, k = ab * j / (S - P);
                                if (0 > f) bb - k > e ? tb.scrollByX(-j) : r(e); else {
                                    if (!(f > 0))return void i();
                                    e > bb + k ? tb.scrollByX(j) : r(e)
                                }
                                c = setTimeout(h, g ? N.initialDelay : N.trackClickRepeatFreq), g = !1
                            }, i = function () {
                                c && clearTimeout(c), c = null, a(document).unbind("mouseup.jsp", i)
                            };
                            return h(), a(document).bind("mouseup.jsp", i), !1
                        }
                    })
                }

                function n() {
                    kb && kb.unbind("mousedown.jsp"), db && db.unbind("mousedown.jsp")
                }

                function o() {
                    a("html").unbind("dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp"), Y && Y.removeClass("jspActive"), _ && _.removeClass("jspActive")
                }

                function p(c, d) {
                    if (W) {
                        0 > c ? c = 0 : c > Z && (c = Z);
                        var e = new a.Event("jsp-will-scroll-y");
                        if (b.trigger(e, [c]), !e.isDefaultPrevented()) {
                            var f = c || 0, g = 0 === f, h = f == Z, i = c / Z, j = -i * (T - Q);
                            void 0 === d && (d = N.animateScroll), d ? tb.animate(Y, "top", c, q, function () {
                                    b.trigger("jsp-user-scroll-y", [-j, g, h])
                                }) : (Y.css("top", c), q(c), b.trigger("jsp-user-scroll-y", [-j, g, h]))
                        }
                    }
                }

                function q(a) {
                    void 0 === a && (a = Y.position().top), R.scrollTop(0), $ = a || 0;
                    var c = 0 === $, d = $ == Z, e = a / Z, f = -e * (T - Q);
                    (ub != c || wb != d) && (ub = c, wb = d, b.trigger("jsp-arrow-change", [ub, wb, vb, xb])), t(c, d), O.css("top", f), b.trigger("jsp-scroll-y", [-f, c, d]).trigger("scroll")
                }

                function r(c, d) {
                    if (X) {
                        0 > c ? c = 0 : c > ab && (c = ab);
                        var e = new a.Event("jsp-will-scroll-x");
                        if (b.trigger(e, [c]), !e.isDefaultPrevented()) {
                            var f = c || 0, g = 0 === f, h = f == ab, i = c / ab, j = -i * (S - P);
                            void 0 === d && (d = N.animateScroll), d ? tb.animate(_, "left", c, s, function () {
                                    b.trigger("jsp-user-scroll-x", [-j, g, h])
                                }) : (_.css("left", c), s(c), b.trigger("jsp-user-scroll-x", [-j, g, h]))
                        }
                    }
                }

                function s(a) {
                    void 0 === a && (a = _.position().left), R.scrollTop(0), bb = a || 0;
                    var c = 0 === bb, d = bb == ab, e = a / ab, f = -e * (S - P);
                    (vb != c || xb != d) && (vb = c, xb = d, b.trigger("jsp-arrow-change", [ub, wb, vb, xb])), u(c, d), O.css("left", f), b.trigger("jsp-scroll-x", [-f, c, d]).trigger("scroll")
                }

                function t(a, b) {
                    N.showArrows && (hb[a ? "addClass" : "removeClass"]("jspDisabled"), ib[b ? "addClass" : "removeClass"]("jspDisabled"))
                }

                function u(a, b) {
                    N.showArrows && (nb[a ? "addClass" : "removeClass"]("jspDisabled"), ob[b ? "addClass" : "removeClass"]("jspDisabled"))
                }

                function v(a, b) {
                    var c = a / (T - Q);
                    p(c * Z, b)
                }

                function w(a, b) {
                    var c = a / (S - P);
                    r(c * ab, b)
                }

                function x(b, c, d) {
                    var e, f, g, h, i, j, k, l, m, n = 0, o = 0;
                    try {
                        e = a(b)
                    } catch (p) {
                        return
                    }
                    for (f = e.outerHeight(), g = e.outerWidth(), R.scrollTop(0), R.scrollLeft(0); !e.is(".jspPane");)if (n += e.position().top, o += e.position().left, e = e.offsetParent(), /^body|html$/i.test(e[0].nodeName))return;
                    h = z(), j = h + Q, h > n || c ? l = n - N.horizontalGutter : n + f > j && (l = n - Q + f + N.horizontalGutter), isNaN(l) || v(l, d), i = y(), k = i + P, i > o || c ? m = o - N.horizontalGutter : o + g > k && (m = o - P + g + N.horizontalGutter), isNaN(m) || w(m, d)
                }

                function y() {
                    return -O.position().left
                }

                function z() {
                    return -O.position().top
                }

                function A() {
                    var a = T - Q;
                    return a > 20 && a - z() < 10
                }

                function B() {
                    var a = S - P;
                    return a > 20 && a - y() < 10
                }

                function C() {
                    R.unbind(zb).bind(zb, function (a, b, c, d) {
                        bb || (bb = 0), $ || ($ = 0);
                        var e = bb, f = $, g = a.deltaFactor || N.mouseWheelSpeed;
                        return tb.scrollBy(c * g, -d * g, !1), e == bb && f == $
                    })
                }

                function D() {
                    R.unbind(zb)
                }

                function E() {
                    return !1
                }

                function F() {
                    O.find(":input,a").unbind("focus.jsp").bind("focus.jsp", function (a) {
                        x(a.target, !1)
                    })
                }

                function G() {
                    O.find(":input,a").unbind("focus.jsp")
                }

                function H() {
                    function c() {
                        var a = bb, b = $;
                        switch (d) {
                            case 40:
                                tb.scrollByY(N.keyboardSpeed, !1);
                                break;
                            case 38:
                                tb.scrollByY(-N.keyboardSpeed, !1);
                                break;
                            case 34:
                            case 32:
                                tb.scrollByY(Q * N.scrollPagePercent, !1);
                                break;
                            case 33:
                                tb.scrollByY(-Q * N.scrollPagePercent, !1);
                                break;
                            case 39:
                                tb.scrollByX(N.keyboardSpeed, !1);
                                break;
                            case 37:
                                tb.scrollByX(-N.keyboardSpeed, !1)
                        }
                        return e = a != bb || b != $
                    }

                    var d, e, f = [];
                    X && f.push(jb[0]), W && f.push(cb[0]), O.bind("focus.jsp", function () {
                        b.focus()
                    }), b.attr("tabindex", 0).unbind("keydown.jsp keypress.jsp").bind("keydown.jsp", function (b) {
                        if (b.target === this || f.length && a(b.target).closest(f).length) {
                            var g = bb, h = $;
                            switch (b.keyCode) {
                                case 40:
                                case 38:
                                case 34:
                                case 32:
                                case 33:
                                case 39:
                                case 37:
                                    d = b.keyCode, c();
                                    break;
                                case 35:
                                    v(T - Q), d = null;
                                    break;
                                case 36:
                                    v(0), d = null
                            }
                            return e = b.keyCode == d && g != bb || h != $, !e
                        }
                    }).bind("keypress.jsp", function (b) {
                        return b.keyCode == d && c(), b.target === this || f.length && a(b.target).closest(f).length ? !e : void 0
                    }), N.hideFocus ? (b.css("outline", "none"), "hideFocus" in R[0] && b.attr("hideFocus", !0)) : (b.css("outline", ""), "hideFocus" in R[0] && b.attr("hideFocus", !1))
                }

                function I() {
                    b.attr("tabindex", "-1").removeAttr("tabindex").unbind("keydown.jsp keypress.jsp"), O.unbind(".jsp")
                }

                function J() {
                    if (location.hash && location.hash.length > 1) {
                        var b, c, d = escape(location.hash.substr(1));
                        try {
                            b = a("#" + d + ', a[name="' + d + '"]')
                        } catch (e) {
                            return
                        }
                        b.length && O.find(d) && (0 === R.scrollTop() ? c = setInterval(function () {
                                R.scrollTop() > 0 && (x(b, !0), a(document).scrollTop(R.position().top), clearInterval(c))
                            }, 50) : (x(b, !0), a(document).scrollTop(R.position().top)))
                    }
                }

                function K() {
                    a(document.body).data("jspHijack") || (a(document.body).data("jspHijack", !0), a(document.body).delegate('a[href*="#"]', "click", function (b) {
                        var c, d, e, f, g, h, i = this.href.substr(0, this.href.indexOf("#")), j = location.href;
                        if (-1 !== location.href.indexOf("#") && (j = location.href.substr(0, location.href.indexOf("#"))), i === j) {
                            c = escape(this.href.substr(this.href.indexOf("#") + 1));
                            try {
                                d = a("#" + c + ', a[name="' + c + '"]')
                            } catch (k) {
                                return
                            }
                            d.length && (e = d.closest(".jspScrollable"), f = e.data("jsp"), f.scrollToElement(d, !0), e[0].scrollIntoView && (g = a(window).scrollTop(), h = d.offset().top, (g > h || h > g + a(window).height()) && e[0].scrollIntoView()), b.preventDefault())
                        }
                    }))
                }

                function L() {
                    var a, b, c, d, e, f = !1;
                    R.unbind("touchstart.jsp touchmove.jsp touchend.jsp click.jsp-touchclick").bind("touchstart.jsp", function (g) {
                        var h = g.originalEvent.touches[0];
                        a = y(), b = z(), c = h.pageX, d = h.pageY, e = !1, f = !0
                    }).bind("touchmove.jsp", function (g) {
                        if (f) {
                            var h = g.originalEvent.touches[0], i = bb, j = $;
                            return tb.scrollTo(a + c - h.pageX, b + d - h.pageY), e = e || Math.abs(c - h.pageX) > 5 || Math.abs(d - h.pageY) > 5, i == bb && j == $
                        }
                    }).bind("touchend.jsp", function () {
                        f = !1
                    }).bind("click.jsp-touchclick", function () {
                        return e ? (e = !1, !1) : void 0
                    })
                }

                function M() {
                    var a = z(), c = y();
                    b.removeClass("jspScrollable").unbind(".jsp"), O.unbind(".jsp"), b.replaceWith(yb.append(O.children())), yb.scrollTop(a), yb.scrollLeft(c), pb && clearInterval(pb)
                }

                var N, O, P, Q, R, S, T, U, V, W, X, Y, Z, $, _, ab, bb, cb, db, eb, fb, gb, hb, ib, jb, kb, lb, mb, nb, ob, pb, qb, rb, sb, tb = this, ub = !0, vb = !0, wb = !1, xb = !1, yb = b.clone(!1, !1).empty(), zb = a.fn.mwheelIntent ? "mwheelIntent.jsp" : "mousewheel.jsp";
                "border-box" === b.css("box-sizing") ? (qb = 0, rb = 0) : (qb = b.css("paddingTop") + " " + b.css("paddingRight") + " " + b.css("paddingBottom") + " " + b.css("paddingLeft"), rb = (parseInt(b.css("paddingLeft"), 10) || 0) + (parseInt(b.css("paddingRight"), 10) || 0)), a.extend(tb, {
                    reinitialise: function (b) {
                        b = a.extend({}, N, b), d(b)
                    }, scrollToElement: function (a, b, c) {
                        x(a, b, c)
                    }, scrollTo: function (a, b, c) {
                        w(a, c), v(b, c)
                    }, scrollToX: function (a, b) {
                        w(a, b)
                    }, scrollToY: function (a, b) {
                        v(a, b)
                    }, scrollToPercentX: function (a, b) {
                        w(a * (S - P), b)
                    }, scrollToPercentY: function (a, b) {
                        v(a * (T - Q), b)
                    }, scrollBy: function (a, b, c) {
                        tb.scrollByX(a, c), tb.scrollByY(b, c)
                    }, scrollByX: function (a, b) {
                        var c = y() + Math[0 > a ? "floor" : "ceil"](a), d = c / (S - P);
                        r(d * ab, b)
                    }, scrollByY: function (a, b) {
                        var c = z() + Math[0 > a ? "floor" : "ceil"](a), d = c / (T - Q);
                        p(d * Z, b)
                    }, positionDragX: function (a, b) {
                        r(a, b)
                    }, positionDragY: function (a, b) {
                        p(a, b)
                    }, animate: function (a, b, c, d, e) {
                        var f = {};
                        f[b] = c, a.animate(f, {
                            duration: N.animateDuration,
                            easing: N.animateEase,
                            queue: !1,
                            step: d,
                            complete: e
                        })
                    }, getContentPositionX: function () {
                        return y()
                    }, getContentPositionY: function () {
                        return z()
                    }, getContentWidth: function () {
                        return S
                    }, getContentHeight: function () {
                        return T
                    }, getPercentScrolledX: function () {
                        return y() / (S - P)
                    }, getPercentScrolledY: function () {
                        return z() / (T - Q)
                    }, getIsScrollableH: function () {
                        return X
                    }, getIsScrollableV: function () {
                        return W
                    }, getContentPane: function () {
                        return O
                    }, scrollToBottom: function (a) {
                        p(Z, a)
                    }, hijackInternalLinks: a.noop, destroy: function () {
                        M()
                    }
                }), d(c)
            }

            return b = a.extend({}, a.fn.jScrollPane.defaults, b), a.each(["arrowButtonSpeed", "trackClickSpeed", "keyboardSpeed"], function () {
                b[this] = b[this] || b.speed
            }), this.each(function () {
                var d = a(this), e = d.data("jsp");
                e ? e.reinitialise(b) : (a("script", d).filter('[type="text/javascript"],:not([type])').remove(), e = new c(d, b), d.data("jsp", e))
            })
        }, a.fn.jScrollPane.defaults = {
            showArrows: !1,
            maintainPosition: !0,
            stickToBottom: !1,
            stickToRight: !1,
            clickOnTrack: !0,
            autoReinitialise: !1,
            autoReinitialiseDelay: 500,
            verticalDragMinHeight: 0,
            verticalDragMaxHeight: 99999,
            horizontalDragMinWidth: 0,
            horizontalDragMaxWidth: 99999,
            contentWidth: void 0,
            animateScroll: !1,
            animateDuration: 300,
            animateEase: "linear",
            hijackInternalLinks: !1,
            verticalGutter: 4,
            horizontalGutter: 4,
            mouseWheelSpeed: 3,
            arrowButtonSpeed: 0,
            arrowRepeatFreq: 50,
            arrowScrollOnHover: !1,
            trackClickSpeed: 0,
            trackClickRepeatFreq: 70,
            verticalArrowPositions: "split",
            horizontalArrowPositions: "split",
            enableKeyboardNavigation: !0,
            hideFocus: !1,
            keyboardSpeed: 0,
            initialDelay: 300,
            speed: 30,
            scrollPagePercent: .8
        }
    });


    // front page - Featured Brand scroll
    $(function () {
        $('.scroll_pane').jScrollPane();
    });

    //Brand List Scroll
    $(function () {
        $('.carousel').carousel({
            interval: 10000
        })
    });
	
	//Custom Select Box - Marketplace
	var x, i, j, selElmnt, selVal, selFilter, a, b, c;
	/*look for any elements with the class "custom-select":*/
	x = document.getElementsByClassName("custom-select");
	for (i = 0; i < x.length; i++) {
		selElmnt = x[i].getElementsByTagName("select")[0];
		selFilter = selElmnt.getAttribute("id");
		selVal = selElmnt.options[selElmnt.selectedIndex].value;
		/*for each element, create a new DIV that will act as the selected item:*/
		a = document.createElement("DIV");
		a.setAttribute("class", "select-selected " + selFilter);
		a.setAttribute("value", selVal);
		a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
		x[i].appendChild(a);
		/*for each element, create a new DIV that will contain the option list:*/
		b = document.createElement("DIV");
		b.setAttribute("class", "select-items select-hide");
		for (j = 1; j < selElmnt.length; j++) {
			/*for each option in the original select element,
			create a new DIV that will act as an option item:*/
			c = document.createElement("DIV");
			c.innerHTML = selElmnt.options[j].innerHTML;
			c.addEventListener("click", function(e) {
				/*when an item is clicked, update the original select box,
				and the selected item:*/
				var y, i, k, s, h, n;
				s = this.parentNode.parentNode.getElementsByTagName("select")[0];
				h = this.parentNode.previousSibling;
				for (i = 0; i < s.length; i++) {
					if (s.options[i].innerHTML == this.innerHTML) {
						s.selectedIndex = i;
						h.innerHTML = this.innerHTML;
						n = s.options[s.selectedIndex].value;
            			h.setAttribute("value", n);
						y = this.parentNode.getElementsByClassName("same-as-selected");
						for (k = 0; k < y.length; k++) {
							y[k].removeAttribute("class");
						}
						this.setAttribute("class", "same-as-selected");
						break;
					}
				}
				h.click();
			});
			b.appendChild(c);
		}
		x[i].appendChild(b);
		a.addEventListener("click", function(e) {
			/*when the select box is clicked, close any other select boxes,
			  and open/close the current select box:*/
			e.stopPropagation();
			closeAllSelect(this);
			this.nextSibling.classList.toggle("select-hide");
			this.classList.toggle("select-arrow-active");
		});
	}
	function closeAllSelect(elmnt) {
		/*a function that will close all select boxes in the document,
		  except the current select box:*/
		var x, y, i, arrNo = [];
		x = document.getElementsByClassName("select-items");
		y = document.getElementsByClassName("select-selected");
		for (i = 0; i < y.length; i++) {
			if (elmnt == y[i]) {
				arrNo.push(i)
			} else {
				y[i].classList.remove("select-arrow-active");
			}
		}
		for (i = 0; i < x.length; i++) {
			if (arrNo.indexOf(i)) {
				x[i].classList.add("select-hide");
			}
		}
	}
	/*if the user clicks anywhere outside the select box,
		then close all select boxes:*/
	document.addEventListener("click", closeAllSelect);
	
	
	//Product masonery - Marketplace
	$(document).ready(function () {
	  var masonryOptions = {
		columnWidth: '.masonry-sizer',
		itemSelector: '.masonry-item',
		percentPosition: true
	  };
	  var $masonryContainer = $('.masonry-container');
	  var masonryBreakpoint = 320; // change this as you wish
	  var masonryActive = false;
	  var activateMasonry = function () {
		if (masonryActive === false) {
		  $masonryContainer.masonry(masonryOptions);
		  masonryActive = true;
		  console.log('bootstrap masonry activated: width ' + $(document).width());
		}
	  };
	  var destroyMasonry = function () {
		if (masonryActive === true) {
		  $masonryContainer.masonry('destroy');
		  masonryActive = false;
		  console.log('bootstrap masonry destroied: width ' + $(document).width());
		}
	  };
	  if ($(document).width() > masonryBreakpoint) {
		activateMasonry();
		$masonryContainer.resize(function () {
		  console.log('bootstrap masonry container resized');
		  if ($(document).width() < masonryBreakpoint) {
			destroyMasonry();
		  } else {
			activateMasonry();
		  }
		});
	  }
	  else {
		console.log('bootstrap masonry not activated: width ' + $(document).width());
	  }
	  $(window).resize(function () {
		if ($(document).width() > masonryBreakpoint) {
		  activateMasonry();
		  $masonryContainer.resize(function () {
			console.log('bootstrap masonry container resized');
			if ($(document).width() < masonryBreakpoint) {
			  destroyMasonry();
			} else {
			  activateMasonry();
			}
		  });
		} else {
		  destroyMasonry();
		}
	  });
	});

})(jQuery); // Fully reference jQuery after this point.


//Landing Page Sticky Header
var floatPanel = new McFloatPanel();
/* Float Panel v2016.10.28. Copyright www.menucool.com */
function McFloatPanel() {
    var i = [], r = [], h = "className", s = "getElementsByClassName", d = "length", l = "display", C = "transition", m = "style", B = "height", c = "scrollTop", k = "offsetHeight", a = "fixed", e = document, b = document.documentElement, j = function (a, c, b) {
        if (a.addEventListener) a.addEventListener(c, b, false); else a.attachEvent && a.attachEvent("on" + c, b)
    }, o = function (c, d) {
        if (typeof getComputedStyle != "undefined")var b = getComputedStyle(c, null); else b = c.currentStyle;
        return b ? b[d] : a
    }, L = function () {
        var a = e.body;
        return Math.max(a.scrollHeight, a[k], b.clientHeight, b.scrollHeight, b[k])
    }, O = function (a, c) {
        var b = a[d];
        while (b--)if (a[b] === c)return true;
        return false
    }, g = function (b, a) {
        return O(b[h].split(" "), a)
    }, q = function (a, b) {
        if (!g(a, b))if (!a[h]) a[h] = b; else a[h] += " " + b
    }, p = function (a, f) {
        if (a[h] && g(a, f)) {
            for (var e = "", c = a[h].split(" "), b = 0, i = c[d]; b < i; b++)if (c[b] !== f) e += c[b] + " ";
            a[h] = e.replace(/^\s+|\s+$/g, "")
        }
    }, n = function () {
        return window.pageYOffset || b[c]
    }, z = function (a) {
        return a.getBoundingClientRect().top
    }, F = function (b) {
        var c = n();
        if (c > b.oS && !g(b, a)) q(b, a); else g(b, a) && c < b.oS && p(b, a)
    }, x = function () {
        for (var a = 0; a < r[d]; a++)J(r[a])
    }, J = function (a) {
        if (a.oS) {
            a.fT && clearTimeout(a.fT);
            a.fT = setTimeout(function () {
                if (a.aF) F(a); else y(a)
            }, 50)
        } else y(a)
    }, w = function (d, c, b) {
        p(d, a);
        c[l] = "none";
        b.position = b.top = ""
    }, y = function (c) {
        var j = z(c), f = c[k], e = c[m], d = c.pH[m], h = n();
        if (j < c.oT && h > c.oS && !g(c, a) && (window.innerHeight || b.clientHeight) > f) {
            c.tP = h + j - c.oT;
            var p = L();
            if (f > p - c.tP - f)var i = f; else i = 0;
            d[l] = "block";
            d[C] = "none";
            d[B] = f + 1 + "px";
            c.pH[k];
            d[C] = "height .3s";
            d[B] = i + "px";
            q(c, a);
            e.position = a;
            e.top = c.oT + "px";
            if (o(c, "position") != a) d[l] = "none"
        } else if (g(c, a) && (h < c.tP || h < c.oS)) {
            var s = o(c, "animation");
            if (c.oS && c.classList && s.indexOf("slide-down") != -1) {
                var r = o(c, "animationDuration");
                c.classList.remove(a);
                e.animationDirection = "reverse";
                e.animationDuration = "300ms";
                void c[k];
                c.classList.add(a);
                setTimeout(function () {
                    w(c, d, e);
                    e.animationDirection = "normal";
                    e.animationDuration = r
                }, 300)
            } else w(c, d, e)
        }
    }, I = function () {
        var f = [], c, b;
        if (e[s]) {
            f = e[s]("float-panel");
            i = e[s]("slideanim")
        } else {
            var k = e.getElementsByTagName("*");
            c = k[d];
            while (c--)g(k[c], "float-panel") && f.push(k[c])
        }
        c = f[d];
        for (var h = 0; h < c; h++) {
            b = r[h] = f[h];
            b.oT = parseInt(b.getAttribute("data-top") || 0);
            b.oS = parseInt(b.getAttribute("data-scroll") || 0);
            if (b.oS > 20 && o(b, "position") == a) b.aF = 1;
            b.pH = e.createElement("div");
            b.pH[m].width = b.offsetWidth + "px";
            b.pH[m][l] = "none";
            b.parentNode.insertBefore(b.pH, b.nextSibling)
        }
        if (r[d]) {
            setTimeout(x, 160);
            j(window, "scroll", x)
        }
    }, f, D = 200, E = 0, u, t, H = function () {
        return window.innerWidth || b.clientWidth || e.body.clientWidth
    };

    function K() {
        u = setInterval(function () {
            var a = e.body;
            if (a[c] < 3) a[c] = 0; else a[c] = a[c] / 1.3;
            if (b[c] < 3) b[c] = 0; else b[c] = b[c] / 1.3;
            if (!n()) {
                clearInterval(u);
                u = null
            }
        }, 14)
    }

    function A() {
        clearTimeout(t);
        if (n() > D && H() > E) {
            t = setTimeout(function () {
                p(f, "mcOut")
            }, 60);
            f[m][l] = "block"
        } else {
            q(f, "mcOut");
            t = setTimeout(function () {
                f[m][l] = "none"
            }, 500)
        }
    }

    var N = function () {
        f = e.getElementById("backtop");
        if (f) {
            var a = f.getAttribute("data-v-w");
            if (a) {
                a = a.replace(/\s/g, "").split(",");
                D = parseInt(a[0]);
                if (a[d] > 1) E = parseInt(a[1])
            }
            j(f, "click", K);
            j(window, "scroll", A);
            A()
        }
    }, v = function () {
        for (var c = n(), e = c + window.innerHeight, g = i[d], b, f, a = 0; a < g; a++) {
            b = c + z(i[a]), f = b + i[a][k];
            if (b < e) q(i[a], "slide"); else p(i[a], "slide")
        }
    }, G = function () {
        if (i[d]) {
            j(window, "scroll", v);
            v()
        }
    }, M = function () {
        I();
        N();
        G()
    };
    j(window, "load", M);
    j(document, "touchstart", function () {
    })
}
