$(function () {
	var miigleLanding = {
		init: function () {
      this.primaryNavigation();
			this.brandsCarousel();
			this.testimonialsCarousel();
			this.video();
			this.tabs();
      this.inputFocus();
			this.noticeDismiss();
			this.newsletterSubscription();
		},

    primaryNavigation: function() {
      $('.navbar-toggle').on('click', function() {
        $('body').toggleClass('navigation-expanded');
      });
      $('body').on('click', function(e) {
        if ($(this).hasClass('navigation-expanded')) {
          $('.primary-nav .navbar-toggle:not(.collapsed)').click();
        }
      });
    },

		brandsCarousel: function() {
			$(".brands-carousel").owlCarousel({
				loop: true,
				autoplay: true,
				autoplayTimeout: 5000,
				autoplaySpeed: 800,
				margin: 60,
				dots: false,
				center: false,
				responsive : {
					0 : {
						items: 1,
						center: true
					},
					480 : {
						items: 2
					},
					768 : {
						items: 4
					},
					1440 : {
						items: 6
					}
				}
			});
		},

		testimonialsCarousel: function() {
			$(".testimonials-carousel").owlCarousel({
				loop: true,
				items: 1,
				autoplay: true,
				autoplayTimeout: 5000,
				autoplaySpeed: 800
			});
		},

		video: function () {
			var instances = plyr.setup({
				enabled: true,
				debug: false
			});

			var i = 1;


			instances.forEach(function (instance) {
				instance.on('ready', function (event) {
					instance.getContainer().setAttribute('id', 'plyr' + i);
					instance.plyId = 'plyr' + i;
					i++;
				});
			});

			$(document).on('opened', '.remodal', function(e) {
				var currentId = $(this).find('.plyr').attr('id');
				instances.forEach(function (instance) {
					if (currentId === instance.plyId) {
						instance.play();
					}
				});
			});

			$(document).on('closed', '.remodal', function(e) {
				instances.forEach(function (instance) {
						instance.pause();
				});
			});
		},

		tabs: function() {
			$('.tabs-bar a').click(function(e) {
				e.preventDefault();
				// Get ID
				var tabID = $(this).attr('href');

				// Remove classes
				$('.tabs-panel').removeClass('active');

				// Add classes
				$(this).parent().addClass('active').siblings().removeClass('active');
				$(tabID).addClass('active');
			});
		},

		inputFocus: function() {
			$('.form-row--btn-right-inside .form-control').focus(function() {
				$(this).parent().addClass('focused');
			});

			$('.form-row--btn-right-inside .form-control').blur(function() {
				$(this).parent().removeClass('focused');
			});
		},

		noticeDismiss: function() {
			if ($('.notice') && !Cookies.get('landing-notice')) {
				$('body').addClass('notice-present');
				$('.notice').addClass('visible');
        $('.primary-nav').css('top', $('.notice').outerHeight()+'px');
			}

      $(window).on('resize', function(e) {
        if ($('.notice') && !Cookies.get('landing-notice')) {
          $('.primary-nav').css('top', $('.notice').outerHeight()+'px');
        } else {
          $('.primary-nav').css('top', '');
        }
      });

			$('.notice__close').click(function() {
				Cookies.set('landing-notice', 'false', { expires: 14 });
				$('body').removeClass('notice-present');
				$(this).parent().remove();
        $('.primary-nav').css('top', '');
			});
		},

		newsletterSubscription: function() {
			/*$('#newsletter-subscribe').submit(function (e) {
				e.preventDefault();

				$.ajax({
					url: '//elpy.us9.list-manage.com/subscribe/post?u=196bccdb031dd42e22e921658&amp;id=83d116968c=?',
					data: $('#newsletter-subscribe').serialize(),
					dataType: 'jsonp',
					success: function (data) {
						console.log(data)
						if (data['result'] !== "success") {
							//ERROR
							console.log(data['msg']);
						} else {
							//SUCCESS - Do what you like here
						}
					}
				});
			});*/
		}
	};

	miigleLanding.init();
});
