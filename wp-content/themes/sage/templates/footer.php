<footer id="footer">
  <div class="links">
    <div class="container">
      <div class="row">
        <?php if( have_rows('footer_link', 'option') ):
            while( have_rows('footer_link','option') ): the_row();
            // vars
            $txt = get_sub_field('footer_title_and_links', 'option');
        ?>
        <div class="col-md-2">
          <?php echo $txt; ?>
        </div>
        <?php endwhile;
              endif;
        ?>
        
		<div class="col-md-4 text-sm-center ml-auto text-white">
			<?php the_field('footer_newsletter', 'option') ?>
        </div>

      </div>
    </div>
  </div>
  
  <div class="copyright text-center">
    &copy; <?php echo date('Y'); ?> MIIGLE
  </div>
</footer>
<script type="text/javascript">
  /* <![CDATA[ */
  var wpHomeUrl = '<?= home_url() ?>';
  /* ]]> */
</script>
<?php the_field('footer_code', 'option') ?>
