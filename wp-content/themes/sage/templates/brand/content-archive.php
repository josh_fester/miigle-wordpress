<?php

  use Miigle\Models\Brand;

  $brands = new WP_Query(array(
    'post_type' => 'mgl_brand',
    'orderby'   => 'title',
    'order'     => 'ASC',
    'posts_per_page'   => 500,
    'tax_query' => array(
      array(
        'taxonomy' => 'mgl_product_category',
        'field'    => 'slug',
        'terms'    => get_query_var('term'),
      ),
    )
  ));
	
?>

<div id="template-brand_detail">
  
  <?php require_once(locate_template('templates/brand/slider-category-desktop.php')); ?>
   
   <section id="brand_detail"> 
    <div class="container"> 
      <div class="row"> 
        <!-- brand detail title section-->
        <div class="col-sm-12 text-center">
          <h1><?= get_query_var('term') ?></h1>
          <p><?= $brands->found_posts ?> brands</p>
        </div>        
      </div>

      <div class="row flexGrid mB">
				<div class="">
				<?php 
				$curr_letter = '';
				while($brands->have_posts()): $brands->the_post();?>
					<?php 
					$this_letter = strtoupper(substr($post->post_title,0,1));
					$the_letter = str_replace(range(0,9), "#", $this_letter);
					if ($the_letter != $curr_letter) {						 
					echo "</div>";
					echo "<div class=\"text-sm-center col\">";
						echo "<div class=\"gridHeader\">";
						echo "<p>$the_letter</p>";
						echo "</div>";
						$curr_letter = $the_letter;
					}
					?>
					<div class="brandGrid borderB">
						<h3>
							<?php if(Brand\is_paid(get_the_ID())): ?>
								<span class="badge"><img src="<?= get_template_directory_uri() ?>/dist/images/pl-badge.png" alt="badge"></span>
							<?php endif; ?>
							<a href="<?= Brand\get_url(get_the_ID()); ?>" target="_blank">
								<?php the_title(); ?> 
							</a>
							<span class="category">
								<?php foreach(Brand\get_brand_categories(get_the_ID()) as $brand): ?>
									<?php if(!$brand->parent): ?>
										<a href="<?= home_url() ?>/category/<?= $brand->name ?>?brands">
											<span class="badge catIcon"><img src="<?= get_template_directory_uri() ?>/dist/images/cat_icon-<?= $brand->name ?>.svg" alt="<?= $brand->name ?>" title="<?= $brand->name ?>"></span>
										</a>
									<?php endif; ?>
								<?php endforeach; ?>
							</span>
						</h3>
						<p><?= Brand\brand_impact(get_the_ID()); ?></p>
						<div class="action">
							<?php if(Brand\brand_cta(get_the_ID()) == 'website'): ?>
								<a href="<?= Brand\get_url(get_the_ID()); ?>" class="btn btn-default btn-cta" target="_blank">VISIT WEBSITE</a>
							<?php elseif(Brand\brand_cta(get_the_ID()) == 'claim_only'): ?>
								<a href="<?= home_url() ?>/brand-submission" class="btn btn-default btn-cta">CLAIM THIS</a>
							<?php else: ?>
								<a href="<?= Brand\get_url(get_the_ID()); ?>" class="btn btn-default btn-cta" target="_blank">VISIT WEBSITE</a>
								<a href="<?= home_url() ?>/brand-submission" class="btn btn-default btn-cta">CLAIM THIS</a>
							<?php endif; ?>
						</div>
					</div>	
				<?php endwhile; ?>
				</div>
      </div>
    </div>
  </section>
  
  <?php require_once(locate_template('templates/brand/slider-category-mobile.php')); ?>
     
</div>
