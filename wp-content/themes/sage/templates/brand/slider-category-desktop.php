<section id="brand_scroll" class="hidden-xs">
		<div class="container">
			<div class="row">        
				<div class="col-sm-12 text-sm-center">
        	<?php the_field('bt_explore_categories_content'); ?>
        </div>
			</div>	
			<div class="row">				
				<div class="col-sm-12 mB">
        	<div id="myCarousel" class="carousel slide">                
						<!-- Carousel items -->
						<div class="carousel-inner">
							<div class="item active">
								<div class="row">
									<div class="col-sm-3">
										<div class="card" style="background-image:url('<?= get_template_directory_uri() ?>/dist/images/brand-accessories.png');">
											<div class="mask accessories"></div>
											<p>Accessories</p>
											<a href="<?= home_url() ?>/category/accessories?brands"><div class="mask"></div></a>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="card" style="background-image:url('<?= get_template_directory_uri() ?>/dist/images/brand-beauty.png');">
											<div class="mask beauty"></div>
											<p>Beauty</p>
											<a href="<?= home_url() ?>/category/beauty?brands"><div class="mask"></div></a>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="card" style="background-image:url('<?= get_template_directory_uri() ?>/dist/images/brand-clothing.png');">
											<div class="mask clothing"></div>
											<p>Clothing</p>
											<a href="<?= home_url() ?>/category/clothing?brands"><div class="mask"></div></a>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="card" style="background-image:url('<?= get_template_directory_uri() ?>/dist/images/brand-electronics.png');">
											<div class="mask electronics"></div>
											<p>Electronics</p>
											<a href="<?= home_url() ?>/category/electronics?brands"><div class="mask"></div></a>
										</div>
									</div>
								</div>
								<!--/row-->
							</div>
							<!--/item-->
							<div class="item">
								<div class="row">
									<div class="col-sm-3">
										<div class="card" style="background-image:url('<?= get_template_directory_uri() ?>/dist/images/brand-foods.png');">
											<div class="mask foods"></div>
											<p>Foods</p>
											<a href="<?= home_url() ?>/category/foods?brands"><div class="mask"></div></a>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="card" style="background-image:url('<?= get_template_directory_uri() ?>/dist/images/brand-gifts.png');">
											<div class="mask gifts"></div>
											<p>Gifts</p>
											<a href="<?= home_url() ?>/category/gifts?brands"><div class="mask"></div></a>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="card" style="background-image:url('<?= get_template_directory_uri() ?>/dist/images/brand-health.png');">
											<div class="mask health"></div>
											<p>Health</p>
											<a href="<?= home_url() ?>/category/health?brands"><div class="mask"></div></a>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="card" style="background-image:url('<?= get_template_directory_uri() ?>/dist/images/brand-home.png');">
											<div class="mask home"></div>
											<p>Home</p>
											<a href="<?= home_url() ?>/category/home?brands"><div class="mask"></div></a>
										</div>
									</div>
								</div>
								<!--/row-->
							</div>
							<!--/item-->
							<div class="item">
								<div class="row">
									<div class="col-sm-3">
										<div class="card" style="background-image:url('<?= get_template_directory_uri() ?>/dist/images/brand-kids.png');">
											<div class="mask kids"></div>
											<p>Kids</p>
											<a href="<?= home_url() ?>/category/kids?brands"><div class="mask"></div></a>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="card" style="background-image:url('<?= get_template_directory_uri() ?>/dist/images/brand-jewelry.png');">
											<div class="mask jewelry"></div>
											<p>Jewelry</p>
											<a href="<?= home_url() ?>/category/jewelry?brands"><div class="mask"></div></a>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="card" style="background-image:url('<?= get_template_directory_uri() ?>/dist/images/brand-pets.png');">
											<div class="mask pets"></div>
											<p>Pets</p>
											<a href="<?= home_url() ?>/category/pets?brands"><div class="mask"></div></a>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="card" style="background-image:url('<?= get_template_directory_uri() ?>/dist/images/brand-shoes.png');">
											<div class="mask shoes"></div>
											<p>Shoes</p>
											<a href="<?= home_url() ?>/category/Shoes?brands"><div class="mask"></div></a>
										</div>
									</div>
								</div>
								<!--/row-->
							</div>
							<!--/item-->    
						</div>
						<!--/carousel-inner-->
						<a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
						<a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
					</div><!--/#myCarousel -->
       	</div>
      </div>
    </div>
   </section>
   <!-- /#brand_scroll for Desktop -->