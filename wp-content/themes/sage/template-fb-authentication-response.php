<?php
if(!session_id()) {
  session_start();
}

echo '<script>document.title = "Miigle+ | Authenticate with Facebook"</script>';

if ( !empty( $_GET['callback'] ) ) { // expects callback url to be present
  write_log( 'Facebook Authentication successfull so far, redirect back successfull, processing access token now.' );
  $callback_url = urldecode($_GET['callback']);
  $chrome_extension_part_pos = strpos($callback_url, 'chrome-extension://');
  $callback_file_part_pos = strpos($callback_url, 'oauth2_callback.html');

  if ( $chrome_extension_part_pos !== false && $callback_file_part_pos !== false ) { // callback url looks valid, continue with authentication
    // include Facebook PHP SDK
    require_once(dirname(__FILE__) . '/vendor/Facebook/autoload.php');

    $fb = new \Facebook\Facebook([
      'app_id' => '119987241907970',
      'app_secret' => 'd2372e46a399627fbfcf9e38987457ac',
      'default_graph_version' => 'v2.9'
    ]);

    $helper = $fb->getRedirectLoginHelper();

    try {
      $accessToken = $helper->getAccessToken();
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
      echo 'ERROR (#8): Authentication through Facebook failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
      write_log( 'ERROR (#8): ' . $e->getMessage() );
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      // When validation fails or other local issues
      echo 'ERROR (#9): Authentication through Facebook failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
      write_log( 'ERROR (#9): ' . $e->getMessage() );
      exit;
    }

    if (! isset($accessToken)) {
      if ($helper->getError()) {
        header('HTTP/1.0 401 Unauthorized');
        echo 'ERROR (#10): Authentication through Facebook failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
        write_log( 'ERROR (#10): ' . $helper->getError() );
        write_log( 'ERROR (#10) Code: ' . $helper->getErrorCode() );
        write_log( 'ERROR (#10) Reason: ' . $helper->getErrorReason() );
        write_log( 'ERROR (#10) Description: ' . $helper->getErrorDescription() );
      } else {
        header('HTTP/1.0 400 Bad Request');
        echo 'ERROR (#11): Authentication through Facebook failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
        write_log( 'ERROR (#11): Something weird going on, access token not received but not Facebook SDK error thrown.' );
      }
      exit;
    }

    // The OAuth 2.0 client handler helps us manage access tokens
    $oAuth2Client = $fb->getOAuth2Client();

    // Get the access token metadata from /debug_token
    $tokenMetadata = $oAuth2Client->debugToken($accessToken);

    write_log( 'Access Token Metadata: ' . print_r( $tokenMetadata, true ) );

    try {
      // Validation (these will throw FacebookSDKException's when they fail)
      $tokenMetadata->validateAppId('119987241907970');
      // If you know the user ID this access token belongs to, you can validate it here
      //$tokenMetadata->validateUserId('123');
      $tokenMetadata->validateExpiration();
    } catch (Facebook\Exceptions\FacebookSDKException $e) {
      echo 'ERROR (#12): Authentication through Facebook failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
      write_log( 'ERROR (#12): Validation of the Facebook Access Token Failed. Reason: ' . $e->getMessage() );
      exit;
    }

    if (! $accessToken->isLongLived()) {
      // Exchanges a short-lived access token for a long-lived one
      try {
        $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
      } catch (Facebook\Exceptions\FacebookSDKException $e) {
        echo 'ERROR (#13): Authentication through Facebook failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
        write_log( 'ERROR (#13): Not able to get long-lived access token. Reason: ' . $e->getMessage() );
        exit;
      }
    }

    $_SESSION['fb_access_token'] = (string) $accessToken;

    try {
      // Returns a `Facebook\FacebookResponse` object
      $response = $fb->get('/me?fields=id,email,first_name,last_name,name', $_SESSION['fb_access_token']);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      echo 'ERROR (#14): Authentication through Facebook failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
      write_log( 'ERROR (#14): Not able to get Facebook user graph data when calling "/me?fields=id,email,first_name,last_name,name" endpoint. Reason: ' . $e->getMessage() );
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      echo 'ERROR (#15): Authentication through Facebook failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
      write_log( 'ERROR (#15): Not able to get Facebook user graph data when calling "/me?fields=id,email,first_name,last_name,name" endpoint. Reason: ' . $e->getMessage() );
      exit;
    }

    try {
      // Proceed knowing you have a logged in user who's authenticated.
      $user_facebook_profile_data = $response->getGraphUser();
      $user_facebook_email = $user_facebook_profile_data->getField('email');

      if ( empty( $user_facebook_profile_data->getId() ) || empty( $user_facebook_profile_data->getName() ) ) {
        echo 'ERROR (#16): Authentication through Facebook failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
        write_log( 'ERROR (#16): Facebook User ID or Name not present.' );
        exit;
      }
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      echo 'ERROR (#17): Authentication through Facebook failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
      write_log( 'ERROR (#17):' . $e->getMessage() );
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      echo 'ERROR (#18): Authentication through Facebook failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
      write_log( 'ERROR (#18):' . $e->getMessage() );
      exit;
    }

    write_log( 'Everything fine with Facebook authentication, moving to WordPress authentication' );

    if (is_user_logged_in()) {
      $current_user = wp_get_current_user();
      write_log( 'User already logged in in WordPress (' . $current_user->ID . ', ' . $current_user->user_login . '), logging out to prevent wrong account linking!' );
      wp_logout();
    }

    if (empty($user_facebook_email)) {
      write_log( 'No facebook email for this user, so create a dummy one.' );
      $user_facebook_email = str_replace(' ', '_', strtolower(sanitize_user($user_facebook_profile_data->getName()))) . '@facebook.com';
    }

    $ID = email_exists($user_facebook_email);

    if ($ID == false) { // user with this email does not exist
      // require_once (ABSPATH . WPINC . '/registration.php');
      write_log( 'User with this email also does not exist' );
      $random_password = wp_generate_password($length = 12, $include_standard_special_chars = false);
      $sanitized_user_login = str_replace(' ', '_', strtolower(sanitize_user($user_facebook_profile_data->getName())));

      if (!validate_username($sanitized_user_login)) {
        $sanitized_user_login = sanitize_user('facebook_' . $user_facebook_profile_data->getId());
      }

      $default_user_name = $sanitized_user_login;
      $i = 1;

      while (username_exists($sanitized_user_login)) {
        $sanitized_user_login = $default_user_name . $i;
        $i++;
      }

      write_log( 'Creating new user:' . $sanitized_user_login . '(' . $user_facebook_email . ')' );
      $ID = wp_create_user($sanitized_user_login, $random_password, $user_facebook_email);

      if (!is_wp_error($ID)) {
        wp_new_user_notification($ID);
        $user_info = get_userdata($ID);
        wp_update_user(array(
          'ID' => $ID,
          'display_name' => $user_facebook_profile_data->getName(),
          'first_name' => $user_facebook_profile_data->getFirstName(),
          'last_name' => $user_facebook_profile_data->getLastName(),
          'facebook' => 'https://www.facebook.com/' . $user_facebook_profile_data->getId()
        ));
      } else {
        echo 'ERROR (#19): Authentication through Facebook failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
        write_log( 'ERROR (#19):' . $ID->get_error_message() );
        exit;
      }
    } else { // user with this email already exists, link his facebook ID
      write_log( 'User with this email (' . $user_facebook_email . ') already exists, new account was not created!' );
    }

    // login
    $secure_cookie = is_ssl();
    $secure_cookie = apply_filters('secure_signon_cookie', $secure_cookie, array());
    global $auth_secure_cookie; // XXX ugly hack to pass this to wp_authenticate_cookie
    $auth_secure_cookie = $secure_cookie;
    wp_set_auth_cookie($ID, true, $secure_cookie);
    $user_info = get_userdata($ID);
    do_action('wp_login', $user_info->user_login, $user_info);
    update_user_meta($ID, 'thechamp_avatar', 'https://graph.facebook.com/' . $user_facebook_profile_data->getId() . '/picture?type=large');
    update_user_meta($ID, 'thechamp_large_avatar', 'https://graph.facebook.com/' . $user_facebook_profile_data->getId() . '/picture?type=large');

    write_log( 'Facebook authentication for ' . $user_facebook_profile_data->getName() . ' (ID: ' . $user_facebook_profile_data->getId() . '), WordPress authentication (ID: ' . $ID . ') all done, now querying for WordPress authentication token.' );

    try {
      // get access token for wp oauth
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,"https://miigle.com/oauth/token");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
        'client_id' => 'VkdpA09M4633hGIQMYG89PPAk5GQmvUiQ7Iq8Rcc',
        'client_secret' => 'YfffWY6JXMR7CGHWMoN9UHrKP9aX1m9ZCpfynmhl',
        'grant_type' => 'client_credentials'
      )));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $server_output = curl_exec ($ch);
      curl_close ($ch);
      // further processing ....
      $server_output = json_decode($server_output);
    } catch (Exception $e) {
      echo 'ERROR (#20): Authentication through Facebook failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
      write_log( 'ERROR (#20):' . $e->getMessage() );
      exit;
    }

    // try {
    //   // get access token for wp oauth
    //   $ch_2 = curl_init();
    //   curl_setopt($ch_2, CURLOPT_URL,"https://www.miigle.com/wp-json/wp/v2/users/".$ID);
    //   $headr = array();
    //   $headr[] = 'Authorization: Bearer '.$server_output->access_token;
    //   curl_setopt($ch_2, CURLOPT_HTTPHEADER,$headr);
    //   curl_setopt($ch_2, CURLOPT_RETURNTRANSFER, true);
    //   $server_output_2 = curl_exec ($ch_2);
    //   curl_close ($ch_2);
    //   // further processing ....
    //   $server_output_2 = json_decode($server_output_2);
    //   write_log( 'Output of the test of token ' . $server_output->access_token . ', on endpoint ' . 'https://www.miigle.com/wp-json/wp/v2/users/' . $ID );
    //   write_log( print_r($server_output_2, true) );
    // } catch (Exception $e) {
    //   echo 'ERROR (#21): Authentication through Facebook failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
    //   write_log( 'ERROR (#21):' . $e->getMessage() );
    //   exit;
    // }

    global $wpdb;
    $is_updated = $wpdb->update( 'wp_oauth_access_tokens', array( 'user_id' => $ID ), array( 'access_token' => $server_output->access_token ), array( '%d' ), array( '%s' ) );
    
    write_log( 'update for access token returned: ' . $is_updated );
    
    write_log( 'All successfully done, doing the redirect now to ' . urldecode( $callback_url ) . '?access_token=' . $server_output->access_token . '&ID=' . $ID . '.' );

    if (!empty($_GET['debug'])) {
      header('Location: ' . get_edit_profile_url());
    } else {
      header('Location: ' . urldecode( $callback_url ) . '?access_token=' . $server_output->access_token . '&ID=' . $ID );
    }

  } else if ( $chrome_extension_part_pos === false ) { // callback not to chrome extension, terminate
    write_log( 'ERROR (#6): callback URL "' . $callback_url . '" is not a valid one, chrome extension protocol not present.' );
    echo 'ERROR (#6): Wrong callback.';
    die();
    exit;
  } else if ( $callback_file_part_pos === false ) { // callback not to oauth2_callback.html page, terminate
    write_log( 'ERROR (#7): callback URL "' . $callback_url . '" is not a valid one, callback page is not the expected one.' );
    echo 'ERROR (#7): Wrong callback.';
    die();
    exit;
  }
} else {
  write_log( 'ERROR (#5): callback URL is missing from the facebook authentication request.' );
  echo 'ERROR (#5): Wrong callback.';
  die();
  exit;
}
?>
