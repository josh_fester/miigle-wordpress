<?php
if(!session_id()) {
  session_start();
}

echo '<script>document.title = "Miigle+ | Authenticate with Facebook"</script>';

if ( !empty( $_GET['callback'] ) ) { // expects callback url to be present
  $callback_url = urldecode($_GET['callback']);
  $chrome_extension_part_pos = strpos($callback_url, 'chrome-extension://');
  $callback_file_part_pos = strpos($callback_url, 'oauth2_callback.html');

  if ( $chrome_extension_part_pos !== false && $callback_file_part_pos !== false ) { // callback url looks valid, continue with authentication
    write_log( '=======================================' );
    write_log( 'Facebook Authentication started' );
    echo 'Authentication in progress, please wait...';

    try {
      // include Facebook PHP SDK
      require_once(dirname(__FILE__) . '/vendor/Facebook/autoload.php');

      $fb = new \Facebook\Facebook([
        'app_id' => '119987241907970',
        'app_secret' => 'd2372e46a399627fbfcf9e38987457ac',
        'default_graph_version' => 'v2.9'
      ]);

      $permissions = ['email', 'public_profile']; // Optional permissions

      $helper = $fb->getRedirectLoginHelper();

      $debug = '';

      if (!empty($_GET['debug'])) {
        $debug = '&debug=true';
      }

      $loginUrl = $helper->getLoginUrl(site_url() . '/fb-authentication-response/?callback=' . urlencode($callback_url) . $debug, $permissions);
      write_log( 'Facebook app permissions OK, callback URL present and valid' );
      write_log( 'Redirecting to ' . $loginUrl );
      echo '<script>window.location="' . $loginUrl . '";</script>';

    } catch (Facebook\Exceptions\FacebookSDKException $e) {
      echo 'ERROR (#4): Authentication through Facebook failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
      write_log( 'ERROR (#4): ' . $e->getMessage() );
      exit;
    }
  } else if ( $chrome_extension_part_pos === false ) { // callback not to chrome extension, terminate
    write_log( 'ERROR (#2): callback URL "' . $callback_url . '" is not a valid one, chrome extension protocol not present.' );
    echo 'ERROR (#2): Wrong callback.';
    die();
    exit;
  } else if ( $callback_file_part_pos === false ) { // callback not to oauth2_callback.html page, terminate
    write_log( 'ERROR (#3): callback URL "' . $callback_url . '" is not a valid one, callback page is not the expected one.' );
    echo 'ERROR (#3): Wrong callback.';
    die();
    exit;
  }
} else {
  write_log( 'ERROR (#1): callback URL is missing from the facebook authentication request.' );
  echo 'ERROR (#1): Wrong callback.';
  die();
  exit;
}
?>
