<?php
if(!session_id()) {
  session_start();
}

echo '<script>document.title = "Miigle+ | Authenticate with Twitter"</script>';

// include Twitter PHP SDK (used version which supports PHP 5.4!)
require_once(dirname(__FILE__) . '/vendor/Twitter/autoload.php');
use Abraham\TwitterOAuth\TwitterOAuth;

if ( !empty( $_GET['callback'] ) ) { // expects callback url to be present
  $callback_url = $_GET['callback'];
  $chrome_extension_part_pos = strpos($callback_url, 'chrome-extension://');
  $callback_file_part_pos = strpos($callback_url, 'oauth2_callback.html');

  if ( $chrome_extension_part_pos !== false && $callback_file_part_pos !== false ) { // callback url looks valid, continue with authentication
    write_log( '=======================================' );
    write_log( 'Twitter Authentication started' );
    echo 'Authentication in progress, please wait...';
    
    try {
      $debug = '';

      if (!empty($_GET['debug'])) {
        $debug = '&debug=true';
      }

      $connection = new TwitterOAuth('YoNqI0IgE9qikpufhxj5V9Aka', 'KMvZRVBU0kHFeZv3cPalW91ad98e5hms5i1yXSEbFhwgK73Fkh');
      $request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => site_url() . '/twt-authentication-response/?callback=' . urlencode($callback_url) . $debug));

      $_SESSION['twt_oauth_token'] = $request_token['oauth_token'];
      $_SESSION['twt_oauth_token_secret'] = $request_token['oauth_token_secret'];

      $loginUrl = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));
      write_log( 'Twitter app permissions OK, callback URL present and valid' );
      write_log( 'Redirecting to ' . $loginUrl );

      echo '<script>window.location="' . $loginUrl . '";</script>';
    } catch (TwitterOAuthException $e) {
      echo 'ERROR (#4): Authentication through Twitter failed to process. We were notified about the error and will resolve it as soon as possible, please try again later.';
      write_log( 'ERROR (#4): ' . $e->getMessage() );
      exit;
    }

  } else if ( $chrome_extension_part_pos === false ) { // callback not to chrome extension, terminate
    write_log( 'ERROR (#2): callback URL "' . $callback_url . '" is not a valid one, chrome extension protocol not present.' );
    echo 'ERROR (#2): Wrong callback.';
    exit;
  } else if ( $callback_file_part_pos === false ) { // callback not to oauth2_callback.html page, terminate
    write_log( 'ERROR (#3): callback URL "' . $callback_url . '" is not a valid one, callback page is not the expected one.' );
    echo 'ERROR (#3): Wrong callback.';
    exit;
  }
} else {
  write_log( 'ERROR (#1): callback URL is missing from the twitter authentication request.' );
  echo 'ERROR (#1): Wrong callback.';
  exit;
}
?>
